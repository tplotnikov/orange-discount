package tim.smedialink.orangesales.gcm;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import tim.smedialink.orangesales.utils.PushService;


public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ComponentName componentName = new ComponentName(context.getPackageName(),
                PushService.class.getName());

        startWakefulService(context, (intent.setComponent(componentName)));
    }
}

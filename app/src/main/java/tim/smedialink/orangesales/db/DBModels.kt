package tim.smedialink.orangesales.db

data class User(
        var access_token: String,
        var first_name: String,
        var last_name: String,
        var gender: String,
        var age: String,
        var city: Map<String, String>,
        var phone: String,
        var email: String,
        var isPartner: String
)

data class Partner(
        var name: String,
        var legalName: String,
        var catalogName: String,
        var inn: String,
        var legalAddress: String,
        var logo: ByteArray,
        var description: String,
        var additions: String,
        var balance: String
)

data class Office(
        var address: String,
        var latitude: String,
        var longitude: String,
        var openTime: String,
        var closeTime: String,
        var phone: String
)

data class Contact(
        var cellPhone: String,
        var cityPhone: String,
        var siteUrl: String,
        var email: String
)


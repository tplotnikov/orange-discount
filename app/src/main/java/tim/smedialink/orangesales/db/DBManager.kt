package tim.smedialink.orangesales.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteDatabase.CursorFactory
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class DBManager(context: Context) : SQLiteOpenHelper(context, "DB", null, 1) {

    val a:Array<String> = Array<String>(1, {"1"} )

    companion object {
        val LOG_TAG = "db_manager"
        val DB_VERSION = 1
        val DB_NAME = "mainDB"
        //Общая информация


        //Таблицы в базе
        val TABLE_USER = "user"
        val TABLE_PARTNER = "partner"
        val TABLE_OFFICES = "offices"
        val TABLE_CONTACTS = "contacts"
        val TABLE_OTHER = "utils"
        /*---------------------------------------*/
        //Общий ключ
        val KEY_ID = "id"

        //Поля таблицы пользователя
        public val KEY_ACCESS_TOKEN = "access_token"
        public val KEY_SOCIAL_TOKEN = "social_token"
        public val KEY_SOCIAL_TYPE = "social_type"
        public val KEY_LASTNAME = "last_name"
        public val KEY_FIRSTNAME = "first_name"
        public val KEY_PHONE = "phone"
        public val KEY_GENDER = "gender"
        public val KEY_AGE = "age"
        public val KEY_CITY = "city_string"

        //Поля таблицы партнера
        public val KEY_NAME = "name"
        public val KEY_LEGALNAME = "legal_name"
        public val KEY_CATALOGNAME = "catalog_name"
        public val KEY_INN = "inn"
        public val KEY_LEGALADDRESS = "legal_address"
        public val KEY_LOGO = "logo"
        public val KEY_DESCRIPTION = "description"
        public val KEY_ADDITIONS = "additions"
        public val KEY_BALANCE = "balance"

        //Для дополнительных таблиц
        public val KEY_PARTNER = "partner_id"
        //Поля таблицы филлиалов
        public val KEY_ADDRESS = "address"
        public val KEY_LATITUDE = "latitude"
        public val KEY_LONGITUDE = "longitude"
        public val KEY_OPENTIME = "open_time"
        public val KEY_CLOSETIME = "close_time"
        public val KEY_FILIALPHONE = "phone"

        //Поля таблицы контактов
        public val KEY_CELLPHONE = "cell_phone"
        public val KEY_CITYPHONE = "city_phone"
        public val KEY_SITE = "site_url"
        public val KEY_EMAIL = "email"
        /*------------------------------------------*/

        //Поля таблицы для всякой херни
        val KEY_UTIL = "key"
        val KEY_VALUE = "value"
    }
/*-----------------------------------------*/

    val CREATE_TABLE_USER =
            "create table $TABLE_USER ($KEY_ID integer primary key, $KEY_ACCESS_TOKEN text, $KEY_SOCIAL_TYPE text, $KEY_SOCIAL_TOKEN text, $KEY_LASTNAME text, $KEY_FIRSTNAME text, $KEY_PHONE text, $KEY_GENDER text, $KEY_AGE text, $KEY_CITY text, $KEY_EMAIL text)"

    val CRATE_TABLE_PARTNER =
            "create table $TABLE_PARTNER ($KEY_ID integer primary key, $KEY_NAME text, $KEY_LEGALNAME text, $KEY_CATALOGNAME text, $KEY_INN text, $KEY_LEGALADDRESS text, $KEY_LOGO blob, $KEY_DESCRIPTION text, $KEY_ADDITIONS text, $KEY_BALANCE text)"

    val CREATE_TABLE_OFFICES =
            "create table $TABLE_OFFICES ($KEY_ID integer primary key, $KEY_PARTNER integer, $KEY_ADDRESS text, $KEY_LATITUDE text, $KEY_LONGITUDE text, $KEY_OPENTIME text, $KEY_CLOSETIME text, $KEY_FILIALPHONE text)"

    val CREATE_TABLE_CONTACTS =
            "create table $TABLE_CONTACTS ($KEY_ID integer primary key, $KEY_PARTNER integer, $KEY_CELLPHONE text, $KEY_CITYPHONE text, $KEY_SITE text, $KEY_EMAIL text)"

    val CREATE_TABLE_OTHER =
            "create table $TABLE_OTHER ($KEY_ID integer primary key, $KEY_UTIL text, $KEY_VALUE text)"

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_TABLE_USER)
        db.execSQL(CRATE_TABLE_PARTNER)
        db.execSQL(CREATE_TABLE_OFFICES)
        db.execSQL(CREATE_TABLE_CONTACTS)

        db.execSQL(CREATE_TABLE_OTHER)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("drop table if exist $TABLE_USER")
        db.execSQL("drop table if exist $TABLE_PARTNER")
        db.execSQL("drop table if exist $TABLE_CONTACTS")
        db.execSQL("drop table if exist $TABLE_OFFICES")
        db.execSQL("drop table if exist $TABLE_OTHER")

        onCreate(db)
    }

    //--------------------------------------------------------
    public fun manageUser(user: User, action: String) {
        val db = this.writableDatabase

        var values: ContentValues = ContentValues()
        values.put(KEY_FIRSTNAME, user.first_name)
        values.put(KEY_LASTNAME, user.last_name)
        values.put(KEY_PHONE, user.phone)
        values.put(KEY_ACCESS_TOKEN, user.access_token)
        values.put(KEY_CITY, user.city["name"])
        values.put(KEY_AGE, user.age)
        values.put(KEY_GENDER, user.gender)
        values.put(KEY_EMAIL, user.email)

        when(action) {
            "create" -> {db.insert(TABLE_USER, null, values)}
            "update" -> {db.update(TABLE_USER, values, "$KEY_ID = ?", a)}
        }

        db.close()
    }


    //--------------------------------------------------------
    public fun managePartner(partner: Partner, action: String) {
        val db = this.writableDatabase

        var values: ContentValues = ContentValues()
        values.put(KEY_NAME, partner.name)
        values.put(KEY_LEGALNAME, partner.legalName)
        values.put(KEY_CATALOGNAME, partner.catalogName)
        values.put(KEY_INN, partner.inn)
        values.put(KEY_LEGALADDRESS, partner.legalAddress)
        values.put(KEY_LOGO, partner.logo)
        values.put(KEY_DESCRIPTION, partner.description)
        values.put(KEY_ADDITIONS, partner.additions)
        values.put(KEY_BALANCE, partner.balance)

        when(action) {
            "create" -> {db.insert(TABLE_PARTNER, null, values)}
            "update" -> {db.update(TABLE_PARTNER, values, "$KEY_ID = ?", a)}
        }
        db.close()
    }


    //--------------------------------------------------------
    public fun manageOffices(office: Office, action: String, id:Int = 1) {
        val db = this.writableDatabase
        var values: ContentValues = ContentValues()

        values.put(KEY_ADDRESS, office.address)
        values.put(KEY_LATITUDE, office.latitude )
        values.put(KEY_LONGITUDE, office.longitude)
        values.put(KEY_OPENTIME, office.openTime )
        values.put(KEY_CLOSETIME, office.closeTime)
        values.put(KEY_FILIALPHONE, office.phone)

        when(action) {
            "create" -> {db.insert(TABLE_OFFICES, null, values)}
            "update" -> {db.update(TABLE_OFFICES, values, "$KEY_ID = ?", arrayOf(id.toString()))}
        }
        db.close()
    }

    //--------------------------------------------------------
    public fun nmanageContacts(contact: Contact, action: String, id:Int = 1) {
        val db = this.writableDatabase
        var values: ContentValues = ContentValues()

        values.put(KEY_CELLPHONE, contact.cellPhone)
        values.put(KEY_CITYPHONE, contact.cityPhone )
        values.put(KEY_SITE, contact.siteUrl)
        values.put(KEY_EMAIL, contact.email)

        when(action) {
            "create" -> {db.insert(TABLE_CONTACTS, null, values)}
            "update" -> {db.update(TABLE_CONTACTS, values, "$KEY_ID = ?", arrayOf(id.toString()))}
        }
        db.close()
    }

    //--------------------------------------------------------
    public fun manageAnyField(table: String, key: String, value: String, action: String, id: Int = 1) {
        val db = this.writableDatabase
        var values: ContentValues = ContentValues()

        values.put(key, value)

        when(action) {
            "update" -> {db.update(table, values, "id = ?", arrayOf(id.toString()))}
            "create" -> {db.insert(table, null, values)}
        }

        db.close()
    }

    public fun manageUtils(key: String, value: String, action: String, id: Int = 1) {
        val db = this.writableDatabase
        var values: ContentValues = ContentValues()

        values.put(KEY_UTIL, key)
        values.put(KEY_VALUE, value)

        when(action) {
            "update" -> {db.update(TABLE_OTHER, values, "id = ?", arrayOf(id.toString()))}
            "create" -> {db.insert(TABLE_OTHER, null, values)}
        }

        db.close()
    }




    public fun getPartner(id: Int = 1) : Partner {
        val db: SQLiteDatabase = this.readableDatabase
        var cursor: Cursor

        val query: String = "select * from $TABLE_PARTNER where id = ${id.toString()}"

        cursor = db.rawQuery(query, null)
        if (cursor.count == 0) {
            throw NullPointerException("no user data in DB")
        } else {
            cursor.moveToFirst()
        }

        val partner = Partner(
                name = cursor.getString(cursor.getColumnIndex(KEY_NAME)),
                legalName = cursor.getString(cursor.getColumnIndex(KEY_LEGALNAME)),
                catalogName = cursor.getString(cursor.getColumnIndex(KEY_CATALOGNAME)),
                inn = cursor.getString(cursor.getColumnIndex(KEY_INN)),
                legalAddress = cursor.getString(cursor.getColumnIndex(KEY_LEGALADDRESS)),
                logo = cursor.getBlob(cursor.getColumnIndex(KEY_LOGO)),
                description = cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)),
                additions = cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)),
                balance = cursor.getString(cursor.getColumnIndex(KEY_BALANCE))
        )

        return partner
    }

    public fun getOffice(id: Int = 1) : Office {
        val db: SQLiteDatabase = this.readableDatabase
        var cursor: Cursor

        val query: String = "select * from $TABLE_OFFICES where id = ${id.toString()}"

        if (db.rawQuery(query, null) == null) {
            throw NullPointerException("no user data in DB")
        } else {
            cursor = db.rawQuery(query, null)
            cursor.moveToFirst()
        }

        val office = Office(
                address = cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)),
                latitude = cursor.getString(cursor.getColumnIndex(KEY_LATITUDE)),
                longitude = cursor.getString(cursor.getColumnIndex(KEY_LONGITUDE)),
                openTime = cursor.getString(cursor.getColumnIndex(KEY_OPENTIME)),
                closeTime = cursor.getString(cursor.getColumnIndex(KEY_CLOSETIME)),
                phone = cursor.getString(cursor.getColumnIndex(KEY_PHONE))
        )

        return office
    }

    public fun getContact(id: Int = 1) : Contact {
        val db: SQLiteDatabase = this.readableDatabase
        var cursor: Cursor

        val query: String = "select * from $TABLE_CONTACTS where id = ${id.toString()}"

        if (db.rawQuery(query, null) == null) {
            throw NullPointerException("no user data in DB")
        } else {
            cursor = db.rawQuery(query, null)
            cursor.moveToFirst()
        }

        val contact = Contact(
                cellPhone = cursor.getString(cursor.getColumnIndex(KEY_CELLPHONE)),
                cityPhone = cursor.getString(cursor.getColumnIndex(KEY_CITYPHONE)),
                siteUrl = cursor.getString(cursor.getColumnIndex(KEY_SITE)),
                email = cursor.getString(cursor.getColumnIndex(KEY_EMAIL))
        )

        return contact
    }

    /*--------------------------------------------------*/

    public fun getUtil(id: Int = 1, key: String) :String {
        val db: SQLiteDatabase = this.readableDatabase
        var cursor: Cursor

        val query: String = "select $KEY_VALUE from $TABLE_OTHER where $KEY_UTIL = \"$key\" and id = ${id.toString()}"
        Log.d("qq", query)

        if (db.rawQuery(query, null) == null) {
            throw NullPointerException("no user data in DB")
        } else {
            cursor = db.rawQuery(query, null)
            cursor.moveToFirst()
        }
        Log.d("cursor", cursor.columnNames.toString())
        return cursor.getString(cursor.getColumnIndex(KEY_VALUE))
    }

}
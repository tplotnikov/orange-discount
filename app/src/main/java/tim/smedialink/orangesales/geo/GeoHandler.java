package tim.smedialink.orangesales.geo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;


public class GeoHandler extends Handler {

    public GoogleMap googleMap;
    private Activity activity;

    private static final int CITY_MSG = 1;
    private static final int COOR_MSG = 201;
    private static final int ADDR_MSG = 202;

    private static String city;
    private static String address;
    private static double latitude;
    private static double longitude;

    public GeoHandler(@Nullable Activity activity) {
        if(activity != null) this.activity = activity;
    }

    @Override
    public void handleMessage(Message message) {
        Bundle bundle = message.getData();
        switch (message.what) {
            case CITY_MSG:
                city = bundle.getString("city");
                break;
            case ADDR_MSG:
                address = bundle.getString("address");
                break;
            case COOR_MSG:
                latitude = bundle.getDouble("lat");
                longitude = bundle.getDouble("lon");
                if (googleMap != null) {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(
                            new LatLng(latitude, longitude)));
                }
                break;
        }
    }

    public static String getCity() {
        return city;
    }

    public static String getAddress() { return address; }

    public static double getLatitude() {
        return latitude;
    }

    public static double getLongitude() {
        return longitude;
    }
}

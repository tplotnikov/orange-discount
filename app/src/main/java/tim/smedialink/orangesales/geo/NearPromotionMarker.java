package tim.smedialink.orangesales.geo;


import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.MinPromotion;

public class NearPromotionMarker implements ClusterItem {
    private MarkerOptions marker;
    private LatLng position;
    private MinPromotion promotion;

    @Override
    public LatLng getPosition() {
        return position;
    }

    public NearPromotionMarker(LatLng pos, MinPromotion promotion) {
        this.position = pos;
        this.promotion = promotion;
    }

    public MarkerOptions getMarker() {
        return this.marker;
    }

    public void setMarker(MarkerOptions marker) {
        this.marker = marker;
    }

    public MinPromotion getPromotion() {
        return promotion;
    }

    public void setPromotion(MinPromotion promotion) {
        this.promotion = promotion;
    }
}

package tim.smedialink.orangesales.geo;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;
import java.util.Locale;

import tim.smedialink.orangesales.utils.Cache;

public class Tracker extends Service implements LocationListener {
    private  Context context;

    private boolean isGpsEnabled = false;
    private boolean isNetworkEnabled = false;
    private boolean canGetLocation = false;

    private Location location;
    private double longitude;
    private double latitude;

    private LocationManager locationManager;

    public Tracker(Context ctx) {
        this.context = ctx;
        getLocation();
    }

    @SuppressWarnings("SecurityException")
    private void getLocation() {
        try {
            locationManager = (LocationManager) context
                    .getSystemService(LOCATION_SERVICE);

            isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGpsEnabled && !isNetworkEnabled) {

            } else {
                this.canGetLocation = true;

                if(isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            60000,
                            10,
                            this);
                    if(locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }

                if (isGpsEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                60000,
                                10, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                Log.d("GPS location", String.valueOf(latitude));
                            }
                        }
                    }
                }
            }
        } catch (SecurityException e) { e.printStackTrace(); }

    }

    public double getLatitude() {
        if(location != null) latitude = location.getLatitude();
        return latitude;
    }

    public double getLongitude() {
        if(location != null) longitude = location.getLongitude();
        return longitude;
    }

    public void getCity(final Handler handler) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    List<Address> addresses = geocoder
                            .getFromLocation(latitude, longitude, 1);
                    if(addresses.size() > 0)
                        result = addresses.get(0).getLocality();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("city", result);
                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("city", "");
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        });
        thread.start();
    }

    public void getCityCoordinates(final Handler handler, @Nullable String name) {
        final String query = name != null ? name : Cache.getUser().city_string;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                double lat = 0;
                double lon = 0;

                try {
                    List<Address> addresses = geocoder
                            .getFromLocationName(query, 1);
                    if(addresses.size() > 0) {
                        lat = addresses.get(0).getLatitude();
                        lon = addresses.get(0).getLongitude();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what = 201;
                    Bundle bundle = new Bundle();
                    bundle.putDouble("lat", lat);
                    bundle.putDouble("lon", lon);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        });
        thread.start();
    }

    public void clean() {
        this.context = null;
        this.locationManager = null;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage("Для работы приложения необходима геолокация.\nВы можете включить ее в настройках");

        alertDialog.setPositiveButton("Настройки", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });

        alertDialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    public void stopUsingGps() {
        try {
            if (locationManager != null)
                locationManager.removeUpdates(Tracker.this);
        } catch (SecurityException e) {}
    }

    @Override
    public void onLocationChanged(Location location) {
        //getCity();
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}

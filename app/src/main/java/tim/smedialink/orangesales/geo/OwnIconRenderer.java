package tim.smedialink.orangesales.geo;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import tim.smedialink.orangesales.R;

public class OwnIconRenderer extends DefaultClusterRenderer<NearPromotionMarker> {

    public OwnIconRenderer(Context context, GoogleMap map,
                           ClusterManager<NearPromotionMarker> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<NearPromotionMarker> cluster, MarkerOptions markerOptions) {
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cluster));
    }

    @Override
    protected void onBeforeClusterItemRendered(NearPromotionMarker item, MarkerOptions markerOptions) {
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));
        super.onBeforeClusterItemRendered(item, markerOptions);
    }
}
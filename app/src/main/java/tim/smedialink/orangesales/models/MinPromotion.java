package tim.smedialink.orangesales.models;


import java.util.ArrayList;

public class MinPromotion {
    private String id;
    private String title;
    private String description;
    private String discount_value;
    private ArrayList<PromPicModel> images;
    private ArrayList<OfficeModel> suboffices;
    private float rank;
    private boolean isFavorite;

    public MinPromotion() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value) {
        this.discount_value = discount_value;
    }

    public ArrayList<PromPicModel> getImages() {
        return images;
    }

    public void setImages(ArrayList<PromPicModel> images) {
        this.images = images;
    }

    public ArrayList<OfficeModel> getSuboffices() {
        if (suboffices.size() > 0) return suboffices;
        else {
            suboffices.add(new OfficeModel());
        }
        return suboffices;
    }

    public void setSuboffices(ArrayList<OfficeModel> suboffices) {
        this.suboffices = suboffices;
    }

    public boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public float getRank() {
        return rank;
    }

    public void setRank(float rank) {
        this.rank = rank;
    }
}

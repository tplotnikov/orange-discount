package tim.smedialink.orangesales.models;

import android.os.Environment;

public class C {

    public static final String USER_DATA = "user_data";

    public static final String SURNAME = "surname";
    public static final String NAME = "name";
    public static final String GENDER = "gender";
    public static final String AGE = "age";
    public static final String PHONE_NUMBER = "phone";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String TOKEN = "token";
    public static final String SOCIAL_TOKEN = "social_token";
    public static final String CITY = "city";

    public static final int PHONE_LEN = 10;
    public static final int PASS_LEN = 6;

    public static final String SERVER_URL = "http://orangesales.tk/orange/web/index.php/v1";
//    public static final String SERVER_URL = "http://192.168.10.71/orange-test/web/index.php/v1";
//    public static final String UPLOADS_BASE_URL = "192.168.10.71/orange-test";
    public static final String UPLOADS_BASE_URL = "http://orangesales.tk/orange";

    public static final String IS_PARTNER = "isPartner";
    public static final String P_DATA = "pData";
    public static final String PARTNER_BALANCE = "pBalance";


}

package tim.smedialink.orangesales.models;


import android.graphics.Bitmap;

public class SubCategoryModel {

    private int id;
    private String name;
    private String imageUrl;
    private Bitmap logo;

    public SubCategoryModel(String mTitle, String mLogo) {
        this.name = mTitle;
        this.imageUrl = mLogo;
    }

    public SubCategoryModel() {
    }

    public String getTitle() {
        return name;
    }

    public void setTitle(String mTitle) {
        this.name = mTitle;
    }

    public String getLogoUrl() {
        return imageUrl;
    }

    public Bitmap getLogo() {
        return logo;
    }

    public void setLogo(Bitmap mLogo) {
        this.logo = mLogo;
    }

    public int getId() {
        return this.id;
    }
}

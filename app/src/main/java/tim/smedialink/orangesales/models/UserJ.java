package tim.smedialink.orangesales.models;

import java.util.HashMap;

public class UserJ {

    public String last_name;
    public String first_name;
    public int gender;
    public int age;
    public String phone;
    public String password;
    public String username;
    public String city_string;
    public String social_type;
    public String social_token;
    public String email;
    public String bonuses;
    public String isPartner;
    public long bonus_expire;

    public UserJ() {}

    public HashMap<String, String> getUser() {
        HashMap<String, String> user = new HashMap<>();
        user.put("last_name", last_name);
        user.put("first_name", first_name);
        user.put("gender", String.valueOf(gender));
        user.put("age", String.valueOf(age));
        user.put("phone", phone);
        user.put("password", password);
        user.put("username", username);
        user.put("city_string", city_string);
        user.put("social_type", social_type);
        user.put("social_token", social_token);
        user.put("email", email);

        return user;
    }
}
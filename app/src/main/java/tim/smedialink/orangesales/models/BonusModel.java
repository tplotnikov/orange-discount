package tim.smedialink.orangesales.models;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;


public class BonusModel extends BaseObservable {

    private int id;
    private String title;
    private String start_date;
    private String end_date;
    private String imageUrl;
    private String price;

    public long endDateTimestamp;

    public boolean isParticipant;

    public BonusModel() {}

    public String getImageUrl() {
        return C.UPLOADS_BASE_URL + imageUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        if(!imageUrl.equals("")) {
            Picasso.with(view.getContext())
                    .load(imageUrl)
                    .into(view);
        } else {

        }
    }
}

package tim.smedialink.orangesales.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import tim.smedialink.orangesales.BR;
import tim.smedialink.orangesales.utils.NormDate;


public class PromotionModel extends BaseObservable {

    private String id;
    private String title;
    private String description;
    private String start_date;
    private String end_date;
    private String plan_expire_date;
    private String discount_value;

    private CompanyModel company;
    private ArrayList<Bitmap> pics;
    private ArrayList<PromPicModel> images;
    private HashMap<String, String> plan;

    public ArrayList<OfficeModel> suboffices;
    public ArrayList<PromVidModel> videos;


    public PromotionModel() {
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @Bindable
    public String getStart_date() {
        return NormDate.makeNorm(start_date);
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }
    @Bindable
    public String getEnd_date() {
        return NormDate.makeNorm(end_date);
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
    @Bindable
    public String getPlan_expire_date() {
        return plan_expire_date;
    }

    public void setPlan_expire_date(String plan_expire_date) {
        this.plan_expire_date = plan_expire_date;
    }
    @Bindable
    public CompanyModel getCompany() {
        return company;
    }

    public void setCompany(CompanyModel company) {
        this.company = company;
    }

    @Bindable
    public HashMap<String, String> getPlan() {
        return plan;
    }

    public void setPlan(HashMap<String, String> plan) {
        this.plan = plan;
    }
    @Bindable
    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value) {
        this.discount_value = discount_value;
        notifyPropertyChanged(BR.discount_value);
    }

    @Bindable
    public ArrayList<Bitmap> getPics() {
        return pics;
    }

    public void setPics(ArrayList<Bitmap> pics) {
        this.pics = pics;
    }

    @Bindable
    public ArrayList<PromPicModel> getImages() {
        if(images.size() == 0) {
            images.add(new PromPicModel());
        }
        return images;
    }

    public void setImages(ArrayList<PromPicModel> images) {

        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

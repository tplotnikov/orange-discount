package tim.smedialink.orangesales.models;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import tim.smedialink.orangesales.utils.Settings;

public class PartnerCompany extends BaseObservable {

    @SerializedName("id")
    public String id = "";


    @SerializedName("name")
    @Bindable
    public String comName = "";

    @Bindable
    @SerializedName("legal_name")
    public String comLegalName = "";

    @Bindable
    @SerializedName("inn")
    public String comInn = "";

    @Bindable
    @SerializedName("legal_address")
    public String comAddr = "";

    public ArrayList<Contact> contacts;
    @Bindable
    public ArrayList<Contact> companyContacts; //Дублирующее поле контактов. Всё из-за Андрюхи


    @Bindable
    transient public String comMPhone = "";
    @Bindable
    transient public String comSPhone = "";
    @Bindable
    transient public String comEmail = "";
    @Bindable
    transient public String comSiteUrl = "";

    transient public Bitmap comLogo;

    @Bindable
    @SerializedName("description")
    public String comDescr = "";

    @Bindable
    @SerializedName("notation")
    public String comAdditions = "";

    @Bindable
    public String balance = "";

    public ArrayList<OfficeModel> addresses;

    @Bindable
    public ArrayList<OfficeModel> suboffices; //опять Андрюха

    public ArrayList<String> cats;
    public float companyRank;

    public PartnerCompany() {}

    transient private Context mContext;

    public void fillWithData(Context ctx, HashMap<String, ?> ets) {
        this.mContext = ctx;
        for(Field f: this.getClass().getFields()) {
            String fName = f.getName();
            if(ets.containsKey(fName)) {
                try {
                    f.set(this, ets.get(fName));
                } catch (Exception e) {e.printStackTrace();}

            }
        }
        fillContacts();
    }

    private void fillContacts() {
        contacts = new ArrayList<>();
        if (comMPhone.isEmpty() || comSPhone.isEmpty() || comEmail.isEmpty()) {
            throw new NullPointerException("Phones and email cant be empty");
        } else {
            contacts.add(new Contact("work_phone", comSPhone));
            contacts.add(new Contact("mobile_phone", comMPhone));
            contacts.add(new Contact("email", comEmail));
            if (!comSiteUrl.isEmpty()) {
                contacts.add(new Contact("website", comSiteUrl));
            }
        }
    }

    public static class Contact {
        public String type;
        public String value;

        public Contact(String type, String value) {
            this.type = type;
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}

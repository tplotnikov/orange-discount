package tim.smedialink.orangesales.models;


import java.util.ArrayList;
import java.util.HashMap;

public class MailoutModel {

    private String id;
    private String message;
    private String description;
    private int type;
    private HashMap<String, ArrayList<String>> conditions;
    private String mail_time;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public HashMap<String, ArrayList<String>> getConditions() {
        return conditions;
    }

    public void setConditions(HashMap<String, ArrayList<String>> conditions) {
        this.conditions = conditions;
    }

    public String getMail_time() {
        return mail_time;
    }

    public void setMail_time(String mail_time) {
        this.mail_time = mail_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

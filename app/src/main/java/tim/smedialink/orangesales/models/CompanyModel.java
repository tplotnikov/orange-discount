package tim.smedialink.orangesales.models;

import android.databinding.BaseObservable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Улучшенная модель компании. @PartnerCompany оставлена для совместимости
 */
public class CompanyModel extends BaseObservable{

    public String id;
    public String name;
    public String legal_name;
    public String inn;
    public String legal_address;
    public String balance;
    public String notation;
    public String description;
    public String logoUrl;

    private float companyRank;

    public ArrayList<OfficeModel> suboffices;
    public ArrayList<ComCatModel> categories;
    public ArrayList<PartnerCompany.Contact> companyContacts;

    public ArrayList<MailoutModel> mailouts;
    public ArrayList<CabPromModel> promotions;

    public ArrayList<String> getCompCats() {
        ArrayList<String> cats = new ArrayList<>();

        for (int i = 0; i < categories.size(); i++) {
            cats.add((String) categories.get(i).category.get("name"));
        }

        return cats;
    }

    public HashMap<String, String> getCompCatsWithIds() {
        HashMap<String, String> result = new HashMap<>();

        for (int i = 0; i < categories.size(); i++) {
            String id = String.valueOf(((Double)categories.get(i).category.get("id")).intValue());
            String name = (String)categories.get(i).category.get("name");
            result.put(id, name);
        }

        return result;
    }

    public String getComContacts(String type) {
        String result = "Не указано";
        for(PartnerCompany.Contact contact: companyContacts) {
            if (contact.type.equals(type)) {
                result = contact.value;
                break;
            }
        }
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegal_name() {
        return legal_name;
    }

    public void setLegal_name(String legal_name) {
        this.legal_name = legal_name;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getLegal_address() {
        return legal_address;
    }

    public void setLegal_address(String legal_address) {
        this.legal_address = legal_address;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getNotation() {
        return notation;
    }

    public void setNotation(String notation) {
        this.notation = notation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getCompanyRank() {

        return companyRank;
    }

    public void setCompanyRank(float companyRank) {
        this.companyRank = companyRank;
    }

    public ArrayList<OfficeModel> getSuboffices() {
        return suboffices;
    }

    public void setSuboffices(ArrayList<OfficeModel> suboffices) {
        this.suboffices = suboffices;
    }



}

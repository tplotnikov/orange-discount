package tim.smedialink.orangesales.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class CategoryModel {
    @SerializedName("name")
    private String name;

    private String description;
    private ArrayList<SubCategoryModel> subcategories;

    public CategoryModel() {}

    public CategoryModel(String title, ArrayList<SubCategoryModel> subcats) {
        this.name = title;
        this.subcategories = subcats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SubCategoryModel> getSubcats() {
        return subcategories;
    }

    public void setSubcats(ArrayList<SubCategoryModel> subcats) {
        this.subcategories = subcats;
    }
}

package tim.smedialink.orangesales.network;
import tim.smedialink.orangesales.models.C;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;


public class RestClient {

    private static final String baseUrl = C.SERVER_URL;
    private static OrangeDiscountApi api;

    public static void init() {

        System.setProperty("http.keepAlive", "false");
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Accept", "application/json");
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(baseUrl)
                .setRequestInterceptor(requestInterceptor)
                .build();

        api = restAdapter.create(OrangeDiscountApi.class);
    }

    public static OrangeDiscountApi getApi() {
        if (api == null) {
            init();
        }
        return api;
    }
}

package tim.smedialink.orangesales.network;


import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.PartMap;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import tim.smedialink.orangesales.models.BonusModel;
import tim.smedialink.orangesales.models.CabPromModel;
import tim.smedialink.orangesales.models.CategoryModel;
import tim.smedialink.orangesales.models.CompanyModel;
import tim.smedialink.orangesales.models.KModels;
import tim.smedialink.orangesales.models.MailoutModel;
import tim.smedialink.orangesales.models.MinPromotion;
import tim.smedialink.orangesales.models.OfficeModel;
import tim.smedialink.orangesales.models.PartnerCompany;
import tim.smedialink.orangesales.models.PriceModel;
import tim.smedialink.orangesales.models.PromotionModel;
import tim.smedialink.orangesales.models.UserJ;
import tim.smedialink.orangesales.utils.Cache;

public interface OrangeDiscountApi {

    @FormUrlEncoded
    @POST("/users/signup")
    void createUser(@FieldMap HashMap<String, String> user,
                    Callback<OrangeCallback> callback);

    @POST("/company")
    void createCompany(@Header("Authorization") String auth,
                       @Body PartnerCompany company,
                       Callback<PartnerCompany> callback);

    @GET("/company?expand=promotions")
    void getCompany(@Header("Authorization") String auth,
                    Callback<CompanyModel> callback);


    @PUT("/company")
    void updateCompany(@Header("Authorization")String auth,
                       @Body HashMap<String, String> data,
                       Callback<Response> callback);

    @Multipart
    @POST("/company/set-logo")
    void setLogo(@Header("Authorization")String auth,
                 @Part("company_id") String id,
                 @Part("logo")TypedFile logo, Callback<Response> callback);

    @POST("/company/suboffice")
    void addOffice(@Header("Authorization")String auth,
                   @Body OfficeModel office,
                   Callback<HashMap<String, String>> callback);

    @PUT("/company/categories")
    void addCat(@Header("Authorization")String auth,
                @Body HashMap<String,ArrayList<String>> ids,
                Callback<HashMap<String, String>> callback);

    @PUT("/company/contact")
    void renewContacts(@Header("Authorization")String auth,
                       @Body HashMap<String,ArrayList<PartnerCompany.Contact>> contacts,
                       Callback<Response> callback);

    @Multipart
    @POST("/users/login")
    void login(@Part("username") String username,
               @Part("password") String password, Callback<OrangeCallback> callback);

    @GET("/profile")
    void getProfile(@Header("Authorization") String token, Callback<UserJ> userCallback);

//    @PUT("/profile")
//    void editProfile(@Header("Authorization") String token,
//                     @Body UserJ user, Callback<Response> profileCallback);

    @Multipart
    @POST("/users/login")
    void soc_login(@Part("social_token") String socToken,
                   @Part("social_type") String socType,
                   Callback<OrangeCallback> isRegistered);

    @GET("/categories")
    void getCats(@Header("Authorization") String auth, Callback<ArrayList<CategoryModel>> callback);

    @GET("/categories/promotions?expand=rank")
    void getPromotion(@Header("Authorization") String auth,
                      @Query("id") String id,
                      Callback<ArrayList<MinPromotion>> callback);
    @GET("/promotions/{id}?expand=company")
    void getOneProm(@Header("" +
            "Authorization") String auth,
                    @Path("id") String promId,
                    Callback<PromotionModel> callback);

    @POST("/promotions/favorite/{id}/toggle")
    void makeFavorite(@Header("Authorization") String auth,
                      @Path("id")String promId,
                      Callback<HashMap<String, String>> callback);

    @GET("/promotions/favorites?expand=rank")
    void getFavorites(@Header("Authorization") String auth,
                      Callback<ArrayList<MinPromotion>> callback);

    @DELETE("/promotions/{id}")
    void removeProm(@Header("Authorization") String auth,
                    @Path("id")String promId,
                    Callback<HashMap<String, String>> callback);

    @Multipart
    @POST("/purchases")
    void makePurchase(@Header("Authorization") String auth,
                      @Part("device_os") String device_os,
                      @Part("data") String purchaseData,
                      @Part("signature") String signature,
                      Callback<HashMap<String, String>> purchaseCallback);

    @Multipart
    @POST("/users/add-push-token")
    void addPushToken(@Header("Authorization") String auth,
                      @Part("type") int type,
                      @Part("token") String pushToken, Callback<Response> responseCallback);

    @GET("/lotteries/market?expand=isParticipant")
    void getLotteries(@Header("Authorization") String auth, Callback<ArrayList<BonusModel>> bonus);

    @PUT("/lotteries/join/{id}")
    void joinLottery(@Header("Authorization") String auth,
                     @Path("id") int lotteryId, Callback<Response> callback);

    @GET("/cities")
    void getCities(@Header("Authorization") String auth,
                   Callback<ArrayList<HashMap<String, String>>> callback);

    @GET("/mailouts/calculate")
    void getMailoutCost(@Header("Authorization") String auth,
                        @Query("type") int type,
                        @Query("conditions[FK_city][]") ArrayList<String> cities,
                        @Query("conditions[age][]") ArrayList<String> ages,
                        @Query("conditions[gender][]") ArrayList<String> gender,
                        Callback<HashMap<String, String>> callback);


    @POST("/mailouts")
    void createMailout(@Header("Authorization") String auth,
                       @Body MailoutModel data,
                       Callback<Response> callback);



    @Multipart
    @POST("/users/forgot-pass")
    void forPass(@Part("phone")String phone, Callback<HashMap<String, String>> callback);

    @Multipart
    @POST("/users/verify-code")
    void verifyCode(@Part("code")String code, Callback<HashMap<String, String>> callback);

    @Multipart
    @POST("/users/change-password")
    void changePassword(@Part("password_reset_token") String passToken,
                        @Part("new_password")String password, Callback<Response> callback);

    @GET("/price-list")
    void getPrices(@Header("Authorization")String auth,
                   Callback<ArrayList<PriceModel>> callback);


    @POST("/promotions")
    void createPromotion(@Header("Authorization") String token,
                         @Body HashMap<String, Object> data,
                         Callback<CabPromModel> callback);

    @Multipart
    @POST("/promotions/resources")
    void addResources(@Header("Authorization") String token,
                      @Part("promotion_id") String promId,
                      @PartMap HashMap<String, TypedFile> images,
                      @PartMap HashMap<String, TypedFile> videos,
                      Callback<Response> callback);

    @GET("/promotions/near?expand=rank")
    void getNearProms(@Header("Authorization") String token,
                      @Nullable @Query("categories") ArrayList<String> ids,
                      Callback<ArrayList<MinPromotion>> callback);

    @GET("/products?sort=value")
    void getProducts(@Header("Authorization") String token,
                     Callback<ArrayList<KModels.Product>> callback);

    @PUT("/profile")
    void editProfile(@Header("Authorization") String token,
                     @Body HashMap<String, String> user,
                     Callback<Response> callback);

    @POST("/profile/bonuses")
    void addBonuses(@Header("Authorization") String token,
                    @Body HashMap<String, String> data,
                    Callback<Response> callback);

    @POST("/company/boost")
    void boost(@Header("Authorization") String token,
               Callback<HashMap<String, String>> callback);


    @GET("/company/{id}")
    void getAnyCompany(@Header("Authorization") String token,
                       @Path("id") String id, Callback<CompanyModel> callback);

    @POST("/company/{id}/evaluate")
    void setCRank(@Header("Authorization") String token,
                  @Path("id") String id,
                  @Body HashMap<String, String> rank,
                  Callback<Response> callback);
}


package tim.smedialink.orangesales.network;


import tim.smedialink.orangesales.db.User;
import tim.smedialink.orangesales.models.UserJ;

public class OrangeCallback {
    public String access_token;
    public String username;
    public String isPartner;
    public UserJ profile;

    public OrangeCallback() {
        this.access_token = "";
        this.username = "";
    }
}

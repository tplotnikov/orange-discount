package tim.smedialink.orangesales.windows;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import tim.smedialink.orangesales.activities.NewPromActivity;
import tim.smedialink.orangesales.activities.SMSSend;

public class MyAlert {

    private static MyAlert instance;

    public static MyAlert getInstance() {
        if(instance == null) {
            instance = new MyAlert();
        }
        return instance;
    }

    public void showMessage(final Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(message)
                .setNeutralButton("Ок", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        try {
                            ((Activity)context).onBackPressed();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

        builder.create().show();
    }

    public void showMsgWithoutClose(final Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(message)
                .setNeutralButton("Ок", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builder.create().show();
    }
}

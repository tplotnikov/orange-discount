package tim.smedialink.orangesales.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.OfficeModel;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.ViewHolder> {

    private ArrayList<String> records;

    public SimpleAdapter(ArrayList<String> items) {
        records = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.new_com_offices_item,
                viewGroup, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
       viewHolder.category.setText(records.get(i));
    }

    public ArrayList<String> getAll() {
        return records;
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public int position;

        private TextView category;

        public ViewHolder(View itemView) {
            super(itemView);
            category = (TextView)itemView.findViewById(R.id.officeListAddress);
            itemView.findViewById(R.id.delOfficeBtn).setVisibility(View.GONE);
        }
    }
}

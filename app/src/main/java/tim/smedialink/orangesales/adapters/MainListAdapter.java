package tim.smedialink.orangesales.adapters;

import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.CategoryModel;
import tim.smedialink.orangesales.utils.LinearLayoutManager;
import tim.smedialink.orangesales.utils.MyItemDecorator;

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ViewHolder> {

    private List<CategoryModel> items;
    private Context mContext;

    public MainListAdapter(Context ctx, List<CategoryModel> items) {
        this.items = items;
        mContext = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.main_list_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.categoryTitle.setText(items.get(i).getName());

        LinearLayoutManager layoutManager1
                = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        SubCatsAdapter subCatsAdapter = new SubCatsAdapter(mContext, items.get(i).getSubcats());
        viewHolder.subCatsList.setLayoutManager(layoutManager1);
        viewHolder.subCatsList.setAdapter(subCatsAdapter);

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView categoryTitle;
        private RecyclerView subCatsList;

        public ViewHolder(View itemView) {
            super(itemView);

            categoryTitle = (TextView)itemView.findViewById(R.id.categoryTitle);
            subCatsList = (RecyclerView)itemView.findViewById(R.id.subCatsList);
            subCatsList.addItemDecoration(new MyItemDecorator(0, 16));

        }
    }
}

package tim.smedialink.orangesales.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.databinding.BonusItemBinding;
import tim.smedialink.orangesales.models.BonusModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;


public class BonusListAdapter extends RecyclerView.Adapter<BonusListAdapter.ViewHolder> {

    private ArrayList<BonusModel> items;
    private Context mContext;

    public BonusListAdapter(Context ctx, ArrayList<BonusModel> items) {
        this.items = items;
        this.mContext = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bonus_item,
                viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.binding.setBonusItem(items.get(i));
        viewHolder.position = i;
        viewHolder.tryWinBtn.setEnabled(!items.get(i).isParticipant);
        long[] exptime = testTimer(i);

        Spannable sDays = new SpannableString(String.format("%s\n Дней", exptime[0]));
        sDays.setSpan(new ForegroundColorSpan(Color.GRAY), 3, sDays.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        Spannable sHours = new SpannableString(String.format("%s\n Часов", exptime[1]));
        sHours.setSpan(new ForegroundColorSpan(Color.GRAY), 3, sHours.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        Spannable sMinutes = new SpannableString(String.format("%s\n Минут", exptime[2]));
        sMinutes.setSpan(new ForegroundColorSpan(Color.GRAY), 3, sMinutes.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        viewHolder.expDays.setText(sDays);
        viewHolder.expHours.setText(sHours);
        viewHolder.expMinutes.setText(sMinutes);
    }

    private long[] testTimer(int i) {

        Date now = Calendar.getInstance().getTime();
        Date exp = new Date(items.get(i).endDateTimestamp * 1000);

        long different = Math.abs(now.getTime() - exp.getTime());

        System.out.println("startDate : " + now);
        System.out.println("endDate : "+ exp);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long[] laja = new long[3];
        laja[0] = elapsedDays;
        laja[1] = elapsedHours;
        laja[2] = elapsedMinutes;

        return laja;
    }



    @Override
    public int getItemCount() {return items.size();}

    class ViewHolder extends RecyclerView.ViewHolder {

        BonusItemBinding binding;
        Button tryWinBtn;

        int position;
        int needBonuses;
        int userBonuses;

        TextView expDays;
        TextView expHours;
        TextView expMinutes;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            tryWinBtn = (Button)itemView.findViewById(R.id.tryWinBtn);

            expDays = (TextView)itemView.findViewById(R.id.exp_days);
            expHours = (TextView)itemView.findViewById(R.id.exp_hours);
            expMinutes = (TextView)itemView.findViewById(R.id.exp_minutes);

            tryWinBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    userBonuses = Integer.parseInt(Cache.getUser().bonuses);
                    needBonuses = Integer.parseInt(items.get(position).getPrice());

                    if(userBonuses >= needBonuses) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                                .setMessage("Вы действительно хотите учавствовать в акции?\nБудет списано " + items.get(position).getPrice() + " бонусов")
                                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        participate();
                                    }
                                })
                                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.create().show();
                    } else {
                        Toast.makeText(mContext, "Недостаточно бонусов", Toast.LENGTH_LONG).show();
                    }
                }
            });


        }

        private void participate() {
            RestClient.getApi().joinLottery(
                    Cache.getAuth(), items.get(position).getId(),
                    new Callback<Response>() {
                        @Override
                        public void success(Response response, Response response2) {
                            tryWinBtn.setText(R.string.already_btn);
                            tryWinBtn.setEnabled(false);
                            Cache.getUser().bonuses = String.valueOf(userBonuses - needBonuses);
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    }
            );
        }
    }
}

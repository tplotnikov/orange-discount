package tim.smedialink.orangesales.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;

import tim.smedialink.orangesales.R;


public class ChooserAdapter extends BaseAdapter {

    private HashMap<String, String> items;
    private String[] mKeys;
    private Context mContext;

    public ChooserAdapter(Context ctx, HashMap<String, String> subCategories) {
        this.items = subCategories;
        this.mContext = ctx;
        this.mKeys = items.keySet().toArray(new String[items.size()]);
    }

    public void toggle(CheckedTextView v) {
        v.setChecked(!v.isChecked());
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(mKeys[position]);
    }

    public String getItemKey(int position) {
        return mKeys[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup group) {

        String key = mKeys[position];
        String value = items.get(key);

        View view;

        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.p_choose_cats_item, group, false);
        } else {
            view = convertView;
        }

        final CheckedTextView ctv = (CheckedTextView)view.findViewById(R.id.pSubCatsList);
        ctv.setText(value);

        return view;
    }

}

package tim.smedialink.orangesales.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.MapsActivity;
import tim.smedialink.orangesales.models.OfficeModel;

public class AddrAdapter extends RecyclerView.Adapter<AddrAdapter.AddrHolder> {

    private ArrayList<OfficeModel> items;
    private Context mContext;

    public AddrAdapter(Context ctx, ArrayList<OfficeModel> items) {
        this.mContext = ctx;
        this.items = items;
    }

    @Override
    public AddrHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stock_addr_item,
                viewGroup, false);
        return new AddrHolder(v);
    }


    @Override
    public void onBindViewHolder(AddrHolder viewHolder, int i) {
        OfficeModel office = items.get(i);
        viewHolder.stockAddr.setText(office.getAddress());
        viewHolder.stockPhone.setText(office.getPhone());

        String fullTime = String.format("с %s до %s", office.getOpenTime(), office.getCloseTime());
        viewHolder.stockTime.setText(fullTime);
        viewHolder.position = i;

    }

    public ArrayList<OfficeModel> getAll() {
        return items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class AddrHolder extends RecyclerView.ViewHolder {

        private TextView stockAddr;
        private TextView stockPhone;
        private TextView stockTime;

        private Button showOnMap;
        private int position;

        public AddrHolder(View itemView) {
            super(itemView);

            stockAddr = (TextView)itemView.findViewById(R.id.stockAddr);
            stockPhone = (TextView)itemView.findViewById(R.id.stockPhone);
            stockTime = (TextView)itemView.findViewById(R.id.stockTime);
            showOnMap = (Button)itemView.findViewById(R.id.button);
            showOnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OfficeModel officeModel = items.get(position);
                    Intent intent = new Intent(mContext, MapsActivity.class);
                    intent.putExtra("lat", officeModel.getLatitude());
                    intent.putExtra("lon", officeModel.getLongitude());
                    intent.putExtra("addr", officeModel.getAddress());
                    mContext.startActivity(intent);
                }
            });
        }
    }
}

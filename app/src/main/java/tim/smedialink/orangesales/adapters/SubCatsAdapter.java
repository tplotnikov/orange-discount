package tim.smedialink.orangesales.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.PromotionsActivity;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.SubCategoryModel;


public class SubCatsAdapter extends RecyclerView.Adapter<SubCatsAdapter.ViewHolder>{

    private ArrayList<SubCategoryModel> subcats;
    private Context mContext;

    public SubCatsAdapter(Context ctx, ArrayList<SubCategoryModel> items) {
        this.subcats = items;
        this.mContext = ctx;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sub_cat_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return subcats.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        viewHolder.position = i;
        viewHolder.categoryTitle.setText(subcats.get(i).getTitle());
        Picasso.with(mContext)
                .load(C.UPLOADS_BASE_URL + subcats.get(i).getLogoUrl())
                .into(viewHolder.categoryLogo);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView categoryTitle;
        private ImageView categoryLogo;

        public int position;

        public ViewHolder(View itemView) {
            super(itemView);

            categoryTitle = (TextView)itemView.findViewById(R.id.subCatTitle);
            categoryLogo = (ImageView)itemView.findViewById(R.id.subCatLogo);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id = String.valueOf(subcats.get(position).getId());
                    String title = subcats.get(position).getTitle();
                    Intent intent = new Intent(mContext, PromotionsActivity.class);
                    intent.putExtra("subCatId", id);
                    intent.putExtra("title", title);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}

package tim.smedialink.orangesales.adapters;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.CabPromModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;

public class CabPromAdapter extends RecyclerView.Adapter<CabPromAdapter.ViewHolder> {

    private ArrayList<CabPromModel> models;

    public CabPromAdapter(ArrayList<CabPromModel> models) {
        this.models = models;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cab_prom_item,
                viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.position = i;
        viewHolder.cabPromDiscount.setText(
                String.format("-%s", models.get(i).discount_value+"%"));
        viewHolder.cabPromName.setText(models.get(i).title);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public int position;

        private TextView cabPromDiscount;
        private TextView cabPromName;
        private ImageView delProm;

        public ViewHolder(View itemView) {
            super(itemView);
            cabPromDiscount = (TextView)itemView.findViewById(R.id.cabPromDiscount);
            cabPromName = (TextView)itemView.findViewById(R.id.cabPromName);
            delProm = (ImageView)itemView.findViewById(R.id.remove_prom_btn);
            delProm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAgree(models.get(position).id);
                }
            });
        }

        private void showAgree(final String id) {
            AlertDialog.Builder builder = new AlertDialog.Builder(itemView.getContext())
                    .setMessage("Вы действительно хотите удалить акцию?")
                    .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            removePromotion(id);
                            models.remove(position);
                            CabPromAdapter.this.notifyItemRemoved(position);
                            CabPromAdapter.this.notifyDataSetChanged();
                        }
                    });
            builder.create().show();
        }

        private void removePromotion(final String id) {
            RestClient.getApi().removeProm(Cache.getAuth(), id,
                    new Callback<HashMap<String, String>>() {
                        @Override
                        public void success(HashMap<String, String> stringStringHashMap, Response response) {
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
        }
    }
}

package tim.smedialink.orangesales.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.MainActivity;
import tim.smedialink.orangesales.fragments.CabinetFragment;
import tim.smedialink.orangesales.models.OfficeModel;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList<OfficeModel> records;
    private Activity activity;

    public RecyclerViewAdapter(Activity ctx, ArrayList<OfficeModel> records) {
        this.records = records;
        this.activity = ctx;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.new_com_offices_item,
                viewGroup, false);
        return new ViewHolder(v);
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.position = i;
        String city = "";
        if (activity instanceof MainActivity) {
            city = records.get(i).cityName;
        } else {
            city = records.get(i).getCity();
        }
        viewHolder.address.setText(
                String.format("%s, %s", city, records.get(i).getAddress()));
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    public ArrayList<OfficeModel> getRecords() {
        return records;
    }

    public void addObjects(OfficeModel model) {
        records.add(model);
        notifyDataSetChanged();
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты.
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        public int position;

        private TextView address;
        private ImageView delBtn;

        public ViewHolder(View itemView) {
            super(itemView);
            address = (TextView) itemView.findViewById(R.id.officeListAddress);
            delBtn = (ImageView) itemView.findViewById(R.id.delOfficeBtn);
            if(activity instanceof MainActivity) {
                delBtn.setVisibility(View.GONE);
            }
            delBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    records.remove(position);
                    notifyItemRemoved(position);
                }
            });
        }
    }
}

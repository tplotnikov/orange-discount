package tim.smedialink.orangesales.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.PromotionsActivity;
import tim.smedialink.orangesales.databinding.PromItemBinding;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.MinPromotion;
import tim.smedialink.orangesales.models.PromotionModel;
import tim.smedialink.orangesales.utils.MyHandlers;

public class PromListAdapter extends RecyclerView.Adapter<PromListAdapter.ViewHolder> {

    private ArrayList<MinPromotion> proms;
    private Context mContext;

    public PromListAdapter(Context ctx, ArrayList<MinPromotion> items) {
        this.proms = items;
        this.mContext = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.prom_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.position = i;

        viewHolder.handlers.id = proms.get(i).getId();
        MinPromotion promotion = proms.get(i);
        viewHolder.binding.setPromotion(promotion);
        viewHolder.binding.setHandlers(viewHolder.handlers);
        if(promotion.getImages().size() > 0) {
            Picasso.with(mContext)
                    .load(C.UPLOADS_BASE_URL + promotion.getImages().get(0).resourceUri)
                    .into(viewHolder.imageLogo);
        }

    }

    @Override
    public int getItemCount() {
        return proms.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageLogo;
        private TextView plRating;
        private RatingBar plRatingBar;
        private MyHandlers handlers;

        public int position;
        public PromItemBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            handlers = new MyHandlers();

            plRating = (TextView)itemView.findViewById(R.id.pl_Rating);
            plRatingBar = (RatingBar)itemView.findViewById(R.id.pl_RatingBar);
            imageLogo = (ImageView)itemView.findViewById(R.id.pl_Logo);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        PromotionsActivity activity = (PromotionsActivity) mContext;
                        activity.choosePromotion(proms.get(position).getId(), false);
                    } catch (Exception e) {
                        Intent intent = new Intent(mContext, PromotionsActivity.class);
                        intent.putExtra("flag", proms.get(position).getId());
                        mContext.startActivity(intent);
                    }
                }
            });
        }
    }
}

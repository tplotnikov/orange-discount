package tim.smedialink.orangesales.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.MailoutModel;

public class CabMailAdapter extends RecyclerView.Adapter<CabMailAdapter.ViewHolder> {

    ArrayList<MailoutModel> records;

    public CabMailAdapter(ArrayList<MailoutModel> models) {
        this.records = models;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cab_mail_item,
                viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        String mailout = records.get(i).getMail_time();
        Date oldDate = null;
        if (mailout != null && !mailout.equals("")) {
            SimpleDateFormat oldftm = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
            try {
                oldDate = oldftm.parse(mailout);
            } catch (Exception e) {
                e.printStackTrace();
            }

            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            String needDate = format.format(oldDate);
            viewHolder.cabMailText.setText("Дата совершения рассылки: "+needDate);
        } else {
            viewHolder.cabMailText.setText("Дата не назначена");
        }


    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public int position;

        private TextView cabMailText;

        public ViewHolder(View itemView) {
            super(itemView);
            cabMailText = (TextView)itemView.findViewById(R.id.cabMailText);
        }
    }
}

package tim.smedialink.orangesales.utils;


import android.view.View;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.network.RestClient;

public class MyHandlers {

    public String id;

    public void makeFavorite(View view) {
        RestClient.getApi().makeFavorite(Cache.getAuth(),
                id, new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> stringStringHashMap, Response response) {

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }


}

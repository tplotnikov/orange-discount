package tim.smedialink.orangesales.utils;


import android.widget.TextView;

import java.util.HashMap;

import tim.smedialink.orangesales.models.PriceModel;

public class PriceCounter {

    private static PriceCounter instance;

    private HashMap<String, PriceModel> prices = new HashMap<>();

    private int freeCats = 0;
    private int freeOffices = 0;

    private TextView targetText;

    public static PriceCounter getInstance() {
        if (instance == null) instance = new PriceCounter();
        return instance;
    }

    public void setPrices(HashMap<String, PriceModel> prices, TextView target) {
        instance.prices = prices;
        freeCats = prices.get("extra_categories").getFree_count();
        freeOffices = prices.get("extra_addresses").getFree_count();
        targetText = target;
    }

    public double calculate(String planId, int cats, int offices) {
        double planPrice = prices.get(planId).getPrice();
        double catsPrice = cats <= freeCats ? 0 : prices.get("extra_categories").getPrice() * (cats - freeCats);
        double officePrice = offices <= freeOffices ? 0 : prices.get("extra_addresses").getPercent() * (offices - freeOffices);

        targetText.setText(String.valueOf(planPrice + catsPrice + officePrice));

        return planPrice + catsPrice + officePrice;
    }
}

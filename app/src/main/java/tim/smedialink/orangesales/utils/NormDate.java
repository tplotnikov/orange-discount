package tim.smedialink.orangesales.utils;


import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import tim.smedialink.orangesales.MyApp;

public class NormDate {

    public static String makeNorm(String date) {
        DateFormat trueFormat = new SimpleDateFormat("dd MMMM yyyy");
        DateFormat originFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date server = originFormat.parse(date);
            return trueFormat.format(server);
        } catch (Exception e) {
            return date;
        }
    }
}

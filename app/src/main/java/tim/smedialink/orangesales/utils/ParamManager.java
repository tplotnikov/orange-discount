package tim.smedialink.orangesales.utils;


import android.util.SparseBooleanArray;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.models.MinPromotion;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.windows.MyAlert;

public class ParamManager {
    private static ParamManager instance;
    private ArrayList<String> params;
    private ArrayList<MinPromotion> promotions;

    private SparseBooleanArray chosenCats;

    public static ParamManager getInstance() {
        if (instance == null) {
            instance = new ParamManager();
        }
        return instance;
    }

    public void setMapParams(ArrayList<String> categoriesIds) {
        this.params = categoriesIds;
    }

    public ArrayList<String> getMapParams() {
        return params;
    }

    @SuppressWarnings("unchecked")
    public void pushParams() {
        if (params != null) {
            RestClient.getApi().getNearProms(Cache.getAuth(), params,
                    new Callback<ArrayList<MinPromotion>>() {
                        @Override
                        public void success(ArrayList<MinPromotion> minPromotions, Response response) {
                            promotions = (ArrayList<MinPromotion>)minPromotions.clone();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                        }
                    });
        }
    }

    public ArrayList<MinPromotion> getResult() {
        if (promotions != null) {
            return promotions;
        } else {
            return new ArrayList<>();
        }
    }

    public void setChosenCats(SparseBooleanArray cats) {
        chosenCats = cats;
    }

    public SparseBooleanArray getChosenCats() {
        if (chosenCats != null)
            return chosenCats;
        else
            return new SparseBooleanArray();
    }


 }

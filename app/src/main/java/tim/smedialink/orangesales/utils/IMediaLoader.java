package tim.smedialink.orangesales.utils;

import java.util.ArrayList;

import retrofit.mime.TypedFile;

/**
 * Created by Timofey Plotnikov on 14.01.16.
 * All rights reserved.
 * Copyright SML.
 */

public interface IMediaLoader {
    ArrayList<TypedFile> getTypedFiles(String[] paths);
    TypedFile makeTypedFile(String path);
}

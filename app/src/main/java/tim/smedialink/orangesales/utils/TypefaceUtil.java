package tim.smedialink.orangesales.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;


public class TypefaceUtil {

    public static void overrideFont(Context context, String defaultFontName, String customFontName) {
        try {
            final Typeface robotoTypeface = Typeface.createFromAsset(context.getAssets(), customFontName);
            final Field defFont = Typeface.class.getDeclaredField(defaultFontName);
            defFont.setAccessible(true);
            defFont.set(null, robotoTypeface);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package tim.smedialink.orangesales.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import tim.smedialink.orangesales.models.CategoryModel;
import tim.smedialink.orangesales.models.SubCategoryModel;

/**
 * CatsParser - утилита для создания общего списка подкатегорий
 */

public class CatsParser {

    private static ArrayList<ArrayList<SubCategoryModel>> allSubCategories = new ArrayList<>();
    private static HashMap<String, String> subCategoriesWithId = new HashMap<>();

    public static void parse(ArrayList<CategoryModel> categories) {
        for (CategoryModel cat: categories) {
            allSubCategories.add(cat.getSubcats());
        }

        for(ArrayList<SubCategoryModel> arrSub: allSubCategories) {
            for(SubCategoryModel subModel: arrSub) {
                subCategoriesWithId.put(String.valueOf(subModel.getId()),
                        subModel.getTitle());
            }
        }
    }

    public static HashMap<String, String> getSubCategoriesWithId() {
        if(subCategoriesWithId != null) {
            return subCategoriesWithId;
        } else {
            return new HashMap<>();
        }
    }

    public static HashMap<String, String> getPartnerCats() {
        ArrayList<String> pCats = Cache.getCompany().getCompCats();
        HashMap<String, String> result = new HashMap<>();
        try {
            if (pCats != null && subCategoriesWithId != null) {
                String[] keys = subCategoriesWithId.keySet().toArray(new String[subCategoriesWithId.size()]);
                for (int i = 0; i < pCats.size(); i++) {
                    if (subCategoriesWithId.containsValue(pCats.get(i))) {
                        result.put(keys[i], subCategoriesWithId.get(keys[i]));
                    }
                }
            } else {
                Log.d("partnercats", "null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}

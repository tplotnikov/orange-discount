package tim.smedialink.orangesales.utils;

import java.io.File;
import java.util.ArrayList;

import retrofit.mime.TypedFile;

/**
 * Created by Timofey Plotnikov on 14.01.16.
 * All rights reserved.
 * Copyright SML.
 */
public class MediaLoader{

    private ArrayList<String> imagePaths;
    private ArrayList<String> videoPaths;

    public MediaLoader() {
        imagePaths = new ArrayList<>();
        videoPaths = new ArrayList<>();
    }

    public String addImagePath(String path) {
        imagePaths.add(path);
        return path;
    }

    public String addVideoPath(String path) {
        videoPaths.add(path);
        return path;
    }

    public ArrayList<TypedFile> makeTypedImages() {
        ArrayList<TypedFile> files = new ArrayList<>();
        if (imagePaths.size() > 0) {
            for (String s: imagePaths) {
                files.add(new TypedFile("image/*", new File(s)));
            }
            return files;
        } else {
            return null;
        }
    }

    public ArrayList<TypedFile> makeTypedVideos() {
        ArrayList<TypedFile> files = new ArrayList<>();
        if (imagePaths.size() > 0) {
            for (String s: videoPaths) {
                files.add(new TypedFile("image/*", new File(s)));
            }
            return files;
        } else {
            return null;
        }
    }

    public int getImageCount() {
        return imagePaths.size();
    }

    public int getVideoCount() {
        return videoPaths.size();
    }
}

package tim.smedialink.orangesales.utils;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.MyApp;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.CompanyModel;
import tim.smedialink.orangesales.models.OfficeModel;
import tim.smedialink.orangesales.models.PartnerCompany;
import tim.smedialink.orangesales.models.UserJ;
import tim.smedialink.orangesales.network.RestClient;


public class Cache {

    private static String mAuth;
    private static UserJ mUser = null;
    private static CompanyModel mCompany = null;

    public static void setUser(UserJ user) {
        mUser = user;
    }
    public static UserJ getUser() { return mUser; }

    public static void setCompany(CompanyModel company) { mCompany = company; }
    public static CompanyModel getCompany() { return mCompany; }

    public static void setAuth(String token) {
        mAuth = String.format("Bearer %s", token);
    }
    public static String getAuth() {
        if (mAuth == null)
            setAuth(Settings.readData(MyApp.getAppContext(), C.TOKEN));
        return mAuth;
    }

    public static void loadDataOnStart(Context context) {
        Gson gson = new GsonBuilder().create();

        try {
            String strUser = Settings.readData(context, C.USER_DATA);
            mUser = gson.fromJson(strUser, UserJ.class);

            String strCompany = Settings.readData(context, C.P_DATA);
            mCompany = gson.fromJson(strCompany, CompanyModel.class);

            setAuth(Settings.readData(context, C.TOKEN));
        } catch (Exception e) {

        }

    }

    public static HashMap<String, String> partnerOffices() {
        HashMap<String, String> result = new HashMap<>();
        for (OfficeModel office: getCompany().getSuboffices()) {
            result.put(office.id, office.getAddress());
        }

        return result;
    }

    public static void renewCompany() {
        RestClient.getApi().getCompany(getAuth(),
                new Callback<CompanyModel>() {
                    @Override
                    public void success(CompanyModel companyModel, Response response) {
                        mCompany = companyModel;
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }
}

package tim.smedialink.orangesales.utils;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.gorbin.asne.core.AccessToken;
import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.core.listener.OnRequestAccessTokenCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.instagram.InstagramSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.MainActivity;
import tim.smedialink.orangesales.activities.SignActivity;
import tim.smedialink.orangesales.fragments.SignUpFragment;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.network.OrangeCallback;
import tim.smedialink.orangesales.network.RestClient;


public class SocialInit extends Fragment implements SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener {

    /**
     * SocialNetwork Ids in ASNE:
     * 1 - Twitter
     * 2 - LinkedIn
     * 3 - Google Plus
     * 4 - Facebook
     * 5 - Vkontakte
     * 6 - Odnoklassniki
     * 7 - Instagram
     */
    public static SocialNetworkManager mSocialNetworkManager;
    private static final String[] sMyScope = new String[]{
            VKScope.FRIENDS,
            VKScope.WALL,
            VKScope.PHOTOS,
            VKScope.NOHTTPS,
            VKScope.MESSAGES,
            VKScope.DOCS
    };

    Button facebookBtn;
    Button vkBtn;
    Button instaBtn;
    Button gplusBtn;

    String socToken;
    String socType;

    public SocialInit() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.social_init_fragment, container, false);
        // init buttons and set Listener

        VKSdk.wakeUpSession(getContext());

        facebookBtn = (Button) rootView.findViewById(R.id.facebookBtn);
        facebookBtn.setOnClickListener(loginClick);
        vkBtn = (Button) rootView.findViewById(R.id.vkBtn);
        vkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VKSdk.login(getActivity(), sMyScope);
            }
        });
        instaBtn = (Button) rootView.findViewById(R.id.instaBtn);
        instaBtn.setOnClickListener(loginClick);
        gplusBtn = (Button) rootView.findViewById(R.id.gplusBtn);
        gplusBtn.setOnClickListener(loginClick);

        //Get Keys for initiate SocialNetworks
        String INSTA_CLIENT_ID = getActivity().getString(R.string.insta_id);
        String INSTA_CLIENT_SECRET = getActivity().getString(R.string.insta_secret);
        String INSTA_CALLBACK_URL = getActivity().getString(R.string.insta_uri);

        //Chose permissions
        ArrayList<String> fbScope = new ArrayList<>();
        fbScope.addAll(Collections.singletonList("public_profile, email, user_friends"));

        //Use manager to manage SocialNetworks
        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(SignActivity.SOCIAL_NETWORK_TAG);

        //Check if manager exist
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);

//            VkSocialNetwork vkNetwork = new VkSocialNetwork(this, VK_APP_ID, sMyScope);
//            mSocialNetworkManager.addSocialNetwork(vkNetwork);

            InstagramSocialNetwork instaNetwork = new InstagramSocialNetwork(this, INSTA_CLIENT_ID, INSTA_CLIENT_SECRET, INSTA_CALLBACK_URL, null);
            mSocialNetworkManager.addSocialNetwork(instaNetwork);

            GooglePlusSocialNetwork gpNetwork = new GooglePlusSocialNetwork(this);
            mSocialNetworkManager.addSocialNetwork(gpNetwork);

            getFragmentManager().beginTransaction().add(mSocialNetworkManager, SignActivity.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if(!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {
                    socialNetwork.setOnLoginCompleteListener(this);
                    initSocialNetwork(socialNetwork);
                }
            }
        }
        return rootView;
    }

    private void initSocialNetwork(SocialNetwork socialNetwork){
//        if(socialNetwork.isConnected()){
//            switch (socialNetwork.getID()){
//                case FacebookSocialNetwork.ID:
//                    facebookBtn.setText("Show Facebook profile");
//                    break;
//                case VkSocialNetwork.ID:
//                    vkBtn.setText("Show Twitter profile");
//                    break;
//                case InstagramSocialNetwork.ID:
//                    instaBtn.setText("Show LinkedIn profile");
//                    break;
//                case GooglePlusSocialNetwork.ID:
//                    gplusBtn.setText("Show GooglePlus profile");
//                    break;
//            }
//        }
    }
    @Override
    public void onSocialNetworkManagerInitialized() {
        //when init SocialNetworks - get and setup login only for initialized SocialNetworks
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            socialNetwork.setOnLoginCompleteListener(this);
            initSocialNetwork(socialNetwork);
        }
    }

    //Login listener

    private View.OnClickListener loginClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int networkId = 0;
            switch (view.getId()){
                case R.id.facebookBtn:
                    networkId = FacebookSocialNetwork.ID;
                    break;
                case R.id.vkBtn:
                    networkId = VkSocialNetwork.ID;
                    break;
                case R.id.instaBtn:
                    networkId = InstagramSocialNetwork.ID;
                    break;
                case R.id.gplusBtn:
                    networkId = GooglePlusSocialNetwork.ID;
                    break;
            }
            SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
            if(!socialNetwork.isConnected()) {
                if(networkId != 0) {
                    socialNetwork.requestLogin(listener);

                } else {
                    Snackbar.make(null, "Wrong networkId", Snackbar.LENGTH_SHORT).show();
                }
            } else {
                startProfile(socialNetwork.getID());
            }
        }
    };

    OnLoginCompleteListener listener = new OnLoginCompleteListener() {
        @Override
        public void onLoginSuccess(int socialNetworkID) {
            if(socialNetworkID == 3) {
                mSocialNetworkManager.getSocialNetwork(socialNetworkID).requestAccessToken(
                        new OnRequestAccessTokenCompleteListener() {
                            @Override
                            public void onRequestAccessTokenComplete(int socialNetworkID, AccessToken accessToken) {
                                socToken = accessToken.token;
                            }

                            @Override
                            public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {

                            }
                        });
            }

            startProfile(socialNetworkID);
        }

        @Override
        public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
            Log.d("VKERROR", errorMessage);
        }
    };

    @Override
    public void onLoginSuccess(int networkId) {

        if(networkId == 3) {
            mSocialNetworkManager.getSocialNetwork(networkId).requestAccessToken(
                    new OnRequestAccessTokenCompleteListener() {
                        @Override
                        public void onRequestAccessTokenComplete(int socialNetworkID, AccessToken accessToken) {
                            socToken = accessToken.token;
                        }

                        @Override
                        public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {

                        }
                    });
        }

        startProfile(networkId);
    }

    @Override
    public void onError(int networkId, String requestID, String errorMessage, Object data) {
    }

    private void startProfile(int networkId){
        if(networkId != 3) {
            socToken = mSocialNetworkManager.getSocialNetwork(networkId).getAccessToken().token;
        }

        socType = null;

        switch (networkId) {
            case 4:
                socType = "fb";
                break;
            case 5:
                socType = "vk";
                break;
            case 7:
                socType = "inst";
                break;
            case 3:
                socType = "gp";
                break;
        }
        RestClient.getApi().soc_login(socToken, socType, new Callback<OrangeCallback>() {
            @Override
            public void success(OrangeCallback orangeCallback, Response response) {
                Settings.saveData(getActivity(), C.TOKEN, orangeCallback.access_token);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }

            @Override
            public void failure(RetrofitError error) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sign_container, new SignUpFragment(), "regFragment")
                        .addToBackStack("login")
                        .commit();
            }
        });
    }
}

package tim.smedialink.orangesales.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Timofey Plotnikov on 11.01.16.
 * All rights reserved.
 * Copyright SML.
 */
public class MyItemDecorator extends RecyclerView.ItemDecoration {
    private final int mVerticalSpaceHeight;
    private final int mHorizontal;

    public MyItemDecorator(int mVerticalSpaceHeight, int horizontal) {
        this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        this.mHorizontal = horizontal;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
            outRect.bottom = mVerticalSpaceHeight;
            outRect.right = mHorizontal;
        }
    }
}

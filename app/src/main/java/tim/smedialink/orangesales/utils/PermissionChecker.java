package tim.smedialink.orangesales.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import tim.smedialink.orangesales.MyApp;

/**
 * Created by Timofey Plotnikov on 13.01.16.
 * All rights reserved.
 * Copyright SML.
 */
public class PermissionChecker {

    private Context mContext;
    private String permission;

    public PermissionChecker(Context context, String perm) {
        mContext = context;
        permission = perm;
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= 21) {
            if (ContextCompat.checkSelfPermission(mContext, permission) != 0) {
                ActivityCompat.requestPermissions((Activity)mContext, new String[] {permission}, 1);
            }
        }
    }
}

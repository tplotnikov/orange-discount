package tim.smedialink.orangesales.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import tim.smedialink.orangesales.activities.MainActivity;
import tim.smedialink.orangesales.activities.PartnerDetail;


public class PushService extends GcmListenerService {

    private String type;
    private String companyId;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        type = data.getString("type");
        String message = data.getString("message");
        String reason = data.getString("reason");
        String title = "";
        companyId = data.getString("company_id");

        if (type != null) {
            switch (type) {
                case "2":
                    title = "Победа!";
                    break;
                case "10":
                    title = "Заявка";
                    break;
                case "20":
                    title = "Рассылка";
                    break;
                default:
                    title = "Оранжевая скидка";
                    break;
            }
        }
        if(message == null) {
            message = "";
        }
        if (reason == null) {
            reason = "";
        }
        Log.d("message", message);

        sendNotification(title, message, reason);
        Cache.renewCompany();
    }

    private void sendNotification(String title, String message, String reason) {
        Intent resultIntent;
        if (!type.equals("1")) {
            resultIntent = new Intent(this, MainActivity.class);
        } else {
            resultIntent = new Intent(this, PartnerDetail.class);
            if(companyId != null) resultIntent.putExtra("companyId", companyId);
        }
        resultIntent.putExtra("type", type);
        resultIntent.putExtra("message", message);
        resultIntent.putExtra("reason", reason);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(message)
                .setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setSmallIcon(android.R.drawable.ic_dialog_email);

        NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        manager.notify(Integer.parseInt(type), builder.build());
    }
}

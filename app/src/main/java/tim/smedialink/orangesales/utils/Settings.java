package tim.smedialink.orangesales.utils;


import android.content.Context;
import android.content.SharedPreferences;

public class Settings {

    private static final String PREFS_FILE = "user_data";

    public static void saveData(Context ctx, String key, String value){
        SharedPreferences.Editor editor = ctx.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String readData(Context ctx, String key){
        SharedPreferences prefs = ctx.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    public static void clearData(Context ctx) {
        SharedPreferences.Editor editor = ctx.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }
}

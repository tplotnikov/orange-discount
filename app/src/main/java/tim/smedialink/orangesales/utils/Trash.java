package tim.smedialink.orangesales.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.PartnerCompany;
import tim.smedialink.orangesales.models.UserJ;


public class Trash {

    public static PartnerCompany getCompany(Context ctx) {
        String json = Settings.readData(ctx, "pData");
        return new Gson().fromJson(json, PartnerCompany.class);
    }

    public static void saveCompany(Context ctx, PartnerCompany company) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(company);
        Settings.saveData(ctx, C.P_DATA, json);
    }

    public static void saveUser(Context ctx, UserJ user) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(user);
        Settings.saveData(ctx, C.USER_DATA, json);
    }
}

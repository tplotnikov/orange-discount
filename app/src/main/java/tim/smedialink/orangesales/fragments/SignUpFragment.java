package tim.smedialink.orangesales.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;

import java.security.Permission;
import java.security.Permissions;
import java.util.ArrayList;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.EULAActivity;
import tim.smedialink.orangesales.activities.MainActivity;
import tim.smedialink.orangesales.activities.SignActivity;
import tim.smedialink.orangesales.geo.CityAdapter;
import tim.smedialink.orangesales.geo.GeoHandler;
import tim.smedialink.orangesales.geo.Tracker;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.UserJ;
import tim.smedialink.orangesales.network.OrangeCallback;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.Drawer;
import tim.smedialink.orangesales.utils.PermissionChecker;
import tim.smedialink.orangesales.utils.Settings;
import tim.smedialink.orangesales.utils.SocialInit;
import tim.smedialink.orangesales.utils.Trash;
import tim.smedialink.orangesales.windows.MyAlert;


public class SignUpFragment extends Fragment
                            implements View.OnClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback {

    private View mainView;
    private EditText editSurname;
    private EditText editName;
    private Spinner genderSpinner;
    private Spinner ageSpinner;
    private EditText editPhone;
    private EditText editCardNumber;
    private EditText editPassword;
    private EditText editEmail;

    private UserJ userJ;
    private String city = "";

    private Tracker gps;

    private ProgressDialog pd;

    private String socType = "";
    private String socToken = "";
    public SignUpFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_sign_up, container, false);
        navDrawerInit();

        //Инициализация формы регистрации
        editSurname = (EditText)mainView.findViewById(R.id.editSurname);
        editName = (EditText)mainView.findViewById(R.id.editName);
        genderSpinner = (Spinner)mainView.findViewById(R.id.genderSpinner);
        ageSpinner = (Spinner)mainView.findViewById(R.id.ageSpinner);
        editPhone = (EditText)mainView.findViewById(R.id.editPhone);
        editPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        editCardNumber = (EditText)mainView.findViewById(R.id.editCardNumber);
        editPassword = (EditText)mainView.findViewById(R.id.editPassword);
        editEmail = (EditText)mainView.findViewById(R.id.editEmail);

        //Утсановка слушателей кнопок
        mainView.findViewById(R.id.register1).setOnClickListener(this);
        mainView.findViewById(R.id.policyCheck).setOnClickListener(this);

        userJ = new UserJ();
        userJ.getUser();

        pd = new ProgressDialog(getContext());
        pd.setMessage("Загрузка...");

        if (!((SignActivity)getActivity()).soc_type.isEmpty() &&
            !((SignActivity)getActivity()).soc_token.isEmpty()) {
            socType = ((SignActivity)getActivity()).soc_type;
            socToken = ((SignActivity)getActivity()).soc_token;
        }

        final CheckedTextView policyCheck = (CheckedTextView)mainView.findViewById(R.id.policyCheck);
        policyCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                policyCheck.setChecked(!policyCheck.isChecked());
                Button register = (Button) mainView.findViewById(R.id.register1);
                register.setEnabled(policyCheck.isChecked());
            }
        });

        TextView userOferta = (TextView)mainView.findViewById(R.id.textView68);
        userOferta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EULAActivity.class);
                intent.putExtra("flag", 1);
                startActivity(intent);
            }
        });

        final CheckedTextView noCardCheck = (CheckedTextView)mainView.findViewById(R.id.noCardCheck);
        noCardCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noCardCheck.setChecked(!noCardCheck.isChecked());
                editCardNumber.setEnabled(!noCardCheck.isChecked());

            }
        });

        return mainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        //Пытаемся определить город чувака
        if (Build.VERSION.SDK_INT <= 21) {
            gps = new Tracker(getContext());
            if (gps.canGetLocation()) {
                gps.getCity(new GeoHandler(getActivity()));
            } else gps.showSettingsAlert();
            onPolicyCheck();
        }
    }

   @Override
   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            if (grantResults[0] == 0) {
                gps = new Tracker(getContext());
                if(gps.canGetLocation()) {
                    gps.getCity(new GeoHandler(getActivity()));
                } else gps.showSettingsAlert();
            }
        }



    //Принял ли чувак пользовательское соглашение
    private void onPolicyCheck() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.policyCheck:
                //onPolicyCheck();
                break;
            case R.id.register1:
                if (fieldsCheck()) {
                    pd.show();
                    registration();
                }
                else Snackbar.make(mainView, getString(R.string.auth_data_fail_hint), Snackbar.LENGTH_SHORT).show();
                break;
        }
    }

    private void registration() {

        city = GeoHandler.getCity();
        //Здесь собираем все введенные данные
        String surname = editSurname.getText().toString();
        String name = editName.getText().toString();

        int gender = genderSpinner.getSelectedItemPosition();
        int age = ageSpinner.getSelectedItemPosition();
        String phone = editPhone.getText().toString().replaceAll("\\D", "");

        //Если номера карты не указано, то он генерируется на сервере
        String cardNumber = editCardNumber.getText().toString();
        String password = editPassword.getText().toString();
        String email = editEmail.getText().toString();

        //Заполнение узера
        userJ.last_name = surname;
        userJ.first_name = name;
        userJ.gender = gender;
        userJ.age = age;
        userJ.phone = phone;
        userJ.username = cardNumber;
        userJ.password = password;
        userJ.email = email;

        if(!socType.isEmpty() && !socToken.isEmpty()) {
            userJ.social_token = socToken;
            userJ.social_type = socType;
        }

        if (city != null && !city .equals("")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("Город");
            alertDialog.setMessage(String.format("Ваш город %s ?", city));

            alertDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    userJ.city_string = city;
                    gps.stopUsingGps();
                    sendUserData();
                }
            });

            alertDialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    makeCityDialog();
                }
            });

            alertDialog.show();
        } else {
            makeCityDialog();
        }
    }

    //Отправляет данные на сервера и при успешной регистрации сохраняет токен и данные
    private void sendUserData() {
        RestClient.getApi().createUser(userJ.getUser(),
                new Callback<OrangeCallback>() {
                    @Override
                    public void success(OrangeCallback orangeCallback, Response response) {
                        Settings.saveData(getContext(), C.TOKEN, orangeCallback.access_token);
                        Settings.saveData(getContext(), C.USERNAME, orangeCallback.username);
                        Settings.saveData(getContext(), "user_pass", userJ.password);
                        Cache.setUser(orangeCallback.profile);
                        Cache.setAuth(orangeCallback.access_token);
                        Trash.saveUser(getContext(), orangeCallback.profile);
                        pd.dismiss();
                        startActivity(new Intent(getContext(), MainActivity.class));
                        getActivity().finish();
                    }

                    @SuppressWarnings("unchecked")
                    @Override
                    public void failure(RetrofitError error) {
                        ArrayList<Map<String, String>> body = (ArrayList<Map<String, String>>)error.getBodyAs(ArrayList.class);
                        String message = body.get(0).get("message");
                        pd.dismiss();
                        Snackbar.make(mainView, message, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    //Проверяет форму регистрации на валидность
    private Boolean fieldsCheck()
    {
        String phh = editPhone.getText().toString().replaceAll("\\D", "");
        return !( (editSurname.getText().toString().equals("")) ||
                (editName.getText().toString().equals(""))  ||
                (phh.length() != C.PHONE_LEN) ||
                (editPassword.getText().length() < C.PASS_LEN ) ||
                (editEmail.getText().toString().equals("")) );
    }

    //Инициализация бокового меню
    private void navDrawerInit() {
        AppCompatActivity mActivity = (AppCompatActivity)getActivity();

        Toolbar toolbar = (Toolbar)mainView.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.reg_activity_title);

        mActivity.setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void makeCityDialog() {

        SimpleDialog.Builder builder = new SimpleDialog.Builder(R.style.Material_App_Dialog_Simple_Light){
            AutoCompleteTextView cityAutoText;
            @Override
            protected void onBuildDone(Dialog dialog) {
                dialog.layoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                cityAutoText = (AutoCompleteTextView)dialog.findViewById(R.id.cityAutoText);
                CityAdapter adapter = new CityAdapter(getContext());
                cityAutoText.setAdapter(adapter);
                cityAutoText.setThreshold(1);
            }

            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                if (cityAutoText.getText().length() > 0 ) {
                    userJ.city_string = cityAutoText.getText().toString();
                    sendUserData();
                    super.onPositiveActionClicked(fragment);
                } else {
                    MyAlert.getInstance().showMsgWithoutClose(getContext(),
                            "Введите город!");
                }

            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
                pd.dismiss();
            }
        };

        builder.positiveAction("ОК")
                .negativeAction("Отмена")
                .contentView(R.layout.choose_city_dialog);

        DialogFragment fragment = DialogFragment.newInstance(builder);

        fragment.show(getActivity().getSupportFragmentManager(), "HH");

    }



}

package tim.smedialink.orangesales.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.MainListAdapter;
import tim.smedialink.orangesales.models.CategoryModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.CatsParser;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {


    private LinearLayoutManager layoutManager;
    private RecyclerView rv;

    private View rootView;

    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_categories, container, false);
        layoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        rv = (RecyclerView)rootView.findViewById(R.id.catalog_list);

        RestClient.getApi().getCats(Cache.getAuth(), new Callback<ArrayList<CategoryModel>>() {
            @Override
            public void success(ArrayList<CategoryModel> categoryModels, Response response) {
                MainListAdapter listAdapter = new MainListAdapter(getContext(), categoryModels);

                rv.setAdapter(listAdapter);
                rv.setLayoutManager(layoutManager);
                rootView.findViewById(R.id.categoriesProgress).setVisibility(View.GONE);
                rootView.findViewById(R.id.catsLayout).setVisibility(View.VISIBLE);
                CatsParser.parse(categoryModels);

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("ERROR", error.getMessage());
            }
        });

        return rootView;
    }
}

package tim.smedialink.orangesales.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.CompanyEditActivity;
import tim.smedialink.orangesales.activities.NewPromActivity;
import tim.smedialink.orangesales.activities.OfficeActivity;
import tim.smedialink.orangesales.activities.SMSSend;
import tim.smedialink.orangesales.adapters.CabMailAdapter;
import tim.smedialink.orangesales.adapters.CabPromAdapter;
import tim.smedialink.orangesales.adapters.RecyclerViewAdapter;
import tim.smedialink.orangesales.adapters.SimpleAdapter;
import tim.smedialink.orangesales.billing.IabHelper;
import tim.smedialink.orangesales.billing.Purchases;
import tim.smedialink.orangesales.databinding.FragmentCabinetBinding;
import tim.smedialink.orangesales.db.Office;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.CompanyModel;
import tim.smedialink.orangesales.models.KModels;
import tim.smedialink.orangesales.models.OfficeModel;
import tim.smedialink.orangesales.models.PartnerCompany;
import tim.smedialink.orangesales.models.PriceModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.LinearLayoutManager;
import tim.smedialink.orangesales.utils.Settings;
import tim.smedialink.orangesales.windows.MyAlert;


public class CabinetFragment extends Fragment {

    public static final int CAB_NEW_OFFICE = 123;

    private LinearLayout fullProfileData;
    private CompanyModel company;
    private ArrayList<OfficeModel> offices;

    private Purchases purchases;

    public FragmentCabinetBinding binding;

    //ITS A MAGIIIIIC!!!!
    private EditText magicEdit;
    private ArrayList<EditText> magicTexts = new ArrayList<>();
    private final int STATE_USUAL = 0;
    private final int STATE_EDIT = 1;
    private int state = 0;

    private PriceModel boost;

    private ArrayList<KModels.Product> products = new ArrayList<>();
    private ArrayList<String> produtcStrings = new ArrayList<>();

    public RecyclerView officeList;
    RecyclerView mailoutsList;
    RecyclerView cabPromsList;

    private Button toTopBtn;
    public CabinetFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_cabinet, container, false);
        binding = DataBindingUtil.bind(view);

        getPProducts();
        getPrice();

        purchases = new Purchases(getActivity());
        purchases.setupHelper();

        company = Cache.getCompany();
        binding.setCompany(company);

        ImageView logo = (ImageView)view.findViewById(R.id.cabPLogo);
        if (company.logoUrl != null) {
            Picasso.with(getContext())
                    .load(C.UPLOADS_BASE_URL + company.logoUrl)
                    .into(logo);
        }
        magicEdit = (EditText)view.findViewById(R.id.magicEdit);
        initMagic(view);

        toTopBtn = (Button)view.findViewById(R.id.to_top_btn);
        if (!Settings.readData(getContext(), "top_expire").equals("")) {
            toTopBtn.setText(Settings.readData(getContext(), "top_expire"));
            toTopBtn.setEnabled(false);
        }

        toTopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveTop();
            }
        });

        fullProfileData = (LinearLayout)view.findViewById(R.id.fullProfileData);
        view.findViewById(R.id.cabFullProfileBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewGroup.LayoutParams lp = fullProfileData.getLayoutParams();
                switch (lp.height) {
                    case 0:
                        ((Button)view).setText("Скрыть");
                        lp.height = -2;
                        break;
                    case -2:
                        ((Button)view).setText("Все данные");
                        lp.height = 0;
                        break;
                }

                fullProfileData.setLayoutParams(lp);
            }
        });

        view.findViewById(R.id.cabEditProfileBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), CompanyEditActivity.class));
            }
        });
        view.findViewById(R.id.addMoneyBtn).setOnClickListener(getOrangeBtn);
        view.findViewById(R.id.cabNewPromBtn).setOnClickListener(newPromBtnClick);
        view.findViewById(R.id.SMSOrderBtn).setOnClickListener(getSms);
        view.findViewById(R.id.addOfficeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivityForResult(new Intent(getActivity(), OfficeActivity.class), CAB_NEW_OFFICE);
            }
        });


        /* ------------------------ */
        officeList = (RecyclerView)view.findViewById(R.id.cabOfficesList);
        RecyclerViewAdapter officeAdapter = new RecyclerViewAdapter(getActivity(), company.suboffices);
        LinearLayoutManager officeManager = new LinearLayoutManager(getContext());
        officeList.setLayoutManager(officeManager);
        officeList.setAdapter(officeAdapter);
        /* ------------------------ */
        RecyclerView cabCatsList = (RecyclerView)view.findViewById(R.id.cabCatsList);
        SimpleAdapter catsAdapter = new SimpleAdapter(company.getCompCats());
        LinearLayoutManager catsManager = new LinearLayoutManager(getContext());
        cabCatsList.setLayoutManager(catsManager);
        cabCatsList.setAdapter(catsAdapter);
        /* ------------------------ */

        setContacts(view);
        setMailouts(view);
        setProms(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.setCompany(Cache.getCompany());
        officeList.getAdapter().notifyDataSetChanged();
        mailoutsList.getAdapter().notifyDataSetChanged();
        cabPromsList.getAdapter().notifyDataSetChanged();

    }
    View.OnClickListener newPromBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(getContext(), NewPromActivity.class));
        }
    };

    View.OnClickListener getOrangeBtn = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            moneyDialog();
        }
    };

    View.OnClickListener getSms = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getContext(), SMSSend.class));
        }
    };

    private void initMagic(View v) {
        magicTexts.add((EditText)v.findViewById(R.id.cabName));
        magicTexts.add((EditText)v.findViewById(R.id.cabLegalName));
        magicTexts.add((EditText)v.findViewById(R.id.cabInn));
        magicTexts.add((EditText)v.findViewById(R.id.cabAddr));
        magicTexts.add((EditText)v.findViewById(R.id.cabMPhone));
        magicTexts.add((EditText)v.findViewById(R.id.cabSPhone));
        magicTexts.add((EditText)v.findViewById(R.id.cabEmail));
        magicTexts.add((EditText) v.findViewById(R.id.cabDescr));
        for (EditText et: magicTexts) {
            et.setKeyListener(null);
        }
    }

    private void editProfile() {
        if (state == STATE_USUAL) {
            for (EditText et: magicTexts) {
                et.setKeyListener(magicEdit.getKeyListener());
            }
            state = STATE_EDIT;
        } else {
            for (EditText et: magicTexts) {
                et.setKeyListener(null);
            }
            sendEdited();
            state = STATE_USUAL;
        }
        Log.wtf("State", String.valueOf(state));
    }

    private void sendEdited() {
        HashMap<String, String> edited = new HashMap<>();
        ArrayList<PartnerCompany.Contact> contacts = new ArrayList<>();
        edited.put("name", magicTexts.get(0).getText().toString());
        edited.put("legal_name", magicTexts.get(1).getText().toString());
        edited.put("inn", magicTexts.get(2).getText().toString());
        edited.put("legal_address", magicTexts.get(3).getText().toString());
        edited.put("description", magicTexts.get(7).getText().toString());

        contacts.add(new PartnerCompany.Contact("mobile_phone", magicTexts.get(4).getText().toString()));
        contacts.add(new PartnerCompany.Contact("work_phone", magicTexts.get(5).getText().toString()));
        contacts.add(new PartnerCompany.Contact("email", magicTexts.get(6).getText().toString()));
        HashMap<String, ArrayList<PartnerCompany.Contact>> data = new HashMap<>();
        data.put("contacts", contacts);

        RestClient.getApi().updateCompany(Cache.getAuth(), edited,
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        MyAlert.getInstance().showMessage(getContext(), "Заявка на обновление данных партнера отправлена");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        MyAlert.getInstance().showMessage(getContext(), "Произошла ошибка");
                    }
                });

        RestClient.getApi().renewContacts(Cache.getAuth(), data,
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });

    }

    private void moneyDialog() {
        View dialogView = getLayoutInflater(null).inflate(R.layout.dialog_layout, null);
        final Spinner skuSpinner = (Spinner)dialogView.findViewById(R.id.skuSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item,
                produtcStrings);
        skuSpinner.setAdapter(adapter);


        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext())
                .setTitle("Пополнение баланса")
                .setView(dialogView)
                .setPositiveButton("Принять", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int sku = skuSpinner.getSelectedItemPosition();
                        purchases.buyItem(products.get(sku).getId());
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = mBuilder.create();
        dialog.show();
    }

    public IabHelper getHelper() {
        return purchases.getMHelper();
    }

    private void getPProducts() {
        RestClient.getApi().getProducts(Cache.getAuth(),
                new Callback<ArrayList<KModels.Product>>() {
                    @Override
                    public void success(ArrayList<KModels.Product> products, Response response) {
                        CabinetFragment.this.products = products;
                        for (KModels.Product pr : products) {
                            produtcStrings.add(pr.getDescription());
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        MyAlert.getInstance().showMessage(getContext(), "Список продуктов не получен.");
                    }
                });
    }

    private void setContacts(View view) {
        ((EditText)view.findViewById(R.id.cabEmail)).setText(binding.getCompany().getComContacts("email"));
        ((EditText)view.findViewById(R.id.cabSPhone)).setText(binding.getCompany().getComContacts("work_phone"));
        ((EditText)view.findViewById(R.id.cabMPhone)).setText(binding.getCompany().getComContacts("mobile_phone"));
    }

    private void setMailouts(View view) {
        mailoutsList = (RecyclerView)view.findViewById(R.id.mailoutsList);
        CabMailAdapter adapter = new CabMailAdapter(company.mailouts);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        mailoutsList.setLayoutManager(manager);
        mailoutsList.setAdapter(adapter);
    }

    private void setProms(View view) {
        cabPromsList = (RecyclerView)view.findViewById(R.id.cabPromotionsList);
        CabPromAdapter adapter = new CabPromAdapter(company.promotions);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        cabPromsList.setLayoutManager(manager);
        cabPromsList.setAdapter(adapter);
    }

    private void getPrice() {
        RestClient.getApi().getPrices(Cache.getAuth(), new Callback<ArrayList<PriceModel>>() {
            @Override
            public void success(ArrayList<PriceModel> priceModels, Response response) {
                seachrBoost(priceModels);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void seachrBoost(ArrayList<PriceModel> models) {
        for (PriceModel m: models) {
            if (m.getId().equals("month_boost")) {
                boost = m;
                break;
            }
        }
    }

    private void moveTop() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setMessage("Данная услуга поднимет все Ваши акции в топ на месяц.\n" +
                        "Стоимость услуги: " + boost.getPrice())
                .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tryToTop();
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    private void tryToTop() {
        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setMessage("Подождите");
        pd.show();
        RestClient.getApi().boost(Cache.getAuth(),
                new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> boostData, Response response) {
                        pd.dismiss();
                        Settings.saveData(getContext(), "top_expire", boostData.get("expireDate"));
                        toTopBtn.setText(boostData.get("expireDate"));
                        toTopBtn.setEnabled(false);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        HashMap<String, String> body = (HashMap<String, String>)error.getBodyAs(HashMap.class);
                        String message = body.get("message");
                        pd.dismiss();
                        MyAlert.getInstance().showMsgWithoutClose(getContext(), message);
                    }
                });
    }
}

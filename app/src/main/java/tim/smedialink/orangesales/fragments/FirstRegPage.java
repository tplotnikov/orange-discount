package tim.smedialink.orangesales.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

import java.util.HashMap;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.NewCompanyActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstRegPage extends Fragment {

    private HashMap<String, EditText> fields;
    private AwesomeValidation mValidator;

    private HashMap<String, Object> data = new HashMap<>();

    public FirstRegPage() {  }

    private void initFields(View v) {
        fields = new HashMap<>();
        fields.put("comName", (EditText)v.findViewById(R.id.e_ComName));
        fields.put("comLegalName", (EditText)v.findViewById(R.id.e_ComLegalName));
        fields.put("comInn", (EditText)v.findViewById(R.id.e_comInn));
        fields.put("comAddr", (EditText) v.findViewById(R.id.e_comAddr));
        fields.put("comMPhone", (EditText)v.findViewById(R.id.e_comMPhone));
        fields.put("comSPhone", (EditText) v.findViewById(R.id.e_comSPhone));
        fields.put("comEmail", (EditText) v.findViewById(R.id.e_comEmail));
        ((EditText) v.findViewById(R.id.e_comSPhone)).addTextChangedListener(new PhoneNumberFormattingTextWatcher());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_first_reg_page, container, false);
        initFields(rootView);

        mValidator = new AwesomeValidation(UNDERLABEL);
        mValidator.setContext(getContext());
        addFuckingValidation();

        ((Button)rootView.findViewById(R.id.pNextBtn)).setOnClickListener(nextClicked);

        return rootView;
    }

    private void addFuckingValidation() {
        mValidator.addValidation(fields.get("comName"), ".{1,100}", getString(R.string.empty_error));
        mValidator.addValidation(fields.get("comLegalName"), ".{1,100}", getString(R.string.empty_error));
        mValidator.addValidation(fields.get("comInn"), "\\d{9,17}", getString(R.string.inn_error));
        mValidator.addValidation(fields.get("comAddr"), ".{1,255}", getString(R.string.empty_error));
        mValidator.addValidation(fields.get("comMPhone"),
                "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", getString(R.string.phone_error));
        mValidator.addValidation(fields.get("comSPhone"),
                "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", getString(R.string.phone_error));
        mValidator.addValidation(fields.get("comEmail"), "^(.+)@(.+)$", getString(R.string.email_error));
    }

    private void moveNext() {
        String[] keys = fields.keySet().toArray(new String[fields.keySet().size()]);
        for(int i = 0; i < fields.size(); i++) {
            data.put(keys[i], fields.get(keys[i]).getText().toString());
        }

        NewCompanyActivity.data = data;
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.p_reg_container, ((NewCompanyActivity)getActivity()).secondRegPage)
                .addToBackStack("preg")
                .commit();
    }

    private View.OnClickListener nextClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Boolean result = mValidator.validate();
            if(!result) {
                Snackbar.make(v, "Неверные данные", Snackbar.LENGTH_LONG).show();
            } else {
                moveNext();
            }
        }
    };


}

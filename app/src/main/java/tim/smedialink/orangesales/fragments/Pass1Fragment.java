package tim.smedialink.orangesales.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.ForgetPassword;
import tim.smedialink.orangesales.network.RestClient;

public class Pass1Fragment extends Fragment {

    private ProgressDialog pd;

    public Pass1Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_pass1, container, false);
        ((EditText)view.findViewById(R.id.editPassPhone)).addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        view.findViewById(R.id.getCodeBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validatePhone(view);
            }
        });

        pd = new ProgressDialog(getContext());
        pd.setMessage("ПОдождите...");
        return view;
    }

    private void validatePhone(final View view) {
        pd.show();
        String phone = ((EditText)view.findViewById(R.id.editPassPhone)).getText().toString();
        if (!phone.equals("")) {
            phone = phone.replaceAll("\\D", "");
            if ((phone.charAt(0) == '8') || (phone.charAt(0) == '7'))
                phone = phone.replaceFirst("7|8", "");


            RestClient.getApi().forPass(phone, new Callback<HashMap<String, String>>() {
                @Override
                public void success(HashMap<String, String> stringStringHashMap, Response response) {
                    if (stringStringHashMap.get("status").equals("1")) {
                        pd.dismiss();
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.pass_container, new Pass2Fragment())
                                .addToBackStack("pass")
                                .commit();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    pd.dismiss();
                    Snackbar.make(view, "Телефон не найден базе", Snackbar.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getContext(), "Введите номер телефона", Toast.LENGTH_SHORT).show();
        }
    }

}

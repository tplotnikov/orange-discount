package tim.smedialink.orangesales.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.PromListAdapter;
import tim.smedialink.orangesales.models.MinPromotion;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.MyItemDecorator;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends Fragment {

    private RecyclerView favoriteList;
    private ProgressDialog pd;
    public FavoritesFragment() { }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        favoriteList = (RecyclerView)view.findViewById(R.id.fav_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        favoriteList.setLayoutManager(layoutManager);
        favoriteList.addItemDecoration(new MyItemDecorator(16, 0));
        pd = new ProgressDialog(getContext());
        pd.setMessage("Загрузка...");

        loadFavorites();

        return view;
    }

    private void loadFavorites() {
        pd.show();
        RestClient.getApi().getFavorites(Cache.getAuth(),
                new Callback<ArrayList<MinPromotion>>() {
                    @Override
                    public void success(ArrayList<MinPromotion> minPromotions, Response response) {
                        PromListAdapter adapter = new PromListAdapter(getContext(), minPromotions);
                        favoriteList.setAdapter(adapter);
                        pd.dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.dismiss();
                    }
                });
    }


}

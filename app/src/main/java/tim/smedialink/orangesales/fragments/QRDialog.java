package tim.smedialink.orangesales.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.utils.QR;
import tim.smedialink.orangesales.utils.Settings;

public class QRDialog extends DialogFragment {

    private ImageView qrImage;

    private BarcodeFormat mFormat;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        switch ((int)args.get("format")) {
            case 11:
                mFormat = BarcodeFormat.QR_CODE;
                break;
            case 4:
                mFormat = BarcodeFormat.CODE_128;
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.qr_dialog, null);
        qrImage = (ImageView)view.findViewById(R.id.qrImage);
        generateQR();

        builder.setView(view)
                .setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }

    private void generateQR() {
        String cardNumber = Settings.readData(getActivity().getBaseContext(),
                C.USERNAME);
        try {
            Bitmap bitmap = QR.encodeAsBitmap(cardNumber, mFormat, 380, 380);
            qrImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

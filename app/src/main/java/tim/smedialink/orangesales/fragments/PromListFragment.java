package tim.smedialink.orangesales.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.PromotionsActivity;
import tim.smedialink.orangesales.adapters.PromListAdapter;
import tim.smedialink.orangesales.models.MinPromotion;
import tim.smedialink.orangesales.models.PromotionModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.MyItemDecorator;


public class PromListFragment extends Fragment {

    private String id;

    private RecyclerView promsList;

    private View rootView;

    public static PromListFragment newInstance() {
        PromListFragment fragment = new PromListFragment();
        return fragment;
    }

    public PromListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = ((PromotionsActivity)getActivity()).subCatId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_prom_list, container, false);

        promsList = (RecyclerView)rootView.findViewById(R.id.promsList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        promsList.setLayoutManager(layoutManager);
        promsList.addItemDecoration(new MyItemDecorator(16, 0));
        getProms(id);
        return rootView;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        promsList = null;
    }

    private void getProms(final String subCatId) {
        RestClient.getApi().getNearProms(Cache.getAuth(), new ArrayList<String>() {{ this.add(subCatId); }},
                new Callback<ArrayList<MinPromotion>>() {
                    @Override
                    public void success(ArrayList<MinPromotion> promotionModels, Response response) {
                        PromListAdapter adapter = new PromListAdapter(getContext(), promotionModels);
                        promsList.setAdapter(adapter);

                        rootView.findViewById(R.id.promsProgress).setVisibility(View.GONE);
                        rootView.findViewById(R.id.promsLayout).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }
}

package tim.smedialink.orangesales.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.CheckCats;
import tim.smedialink.orangesales.activities.MapsActivity;
import tim.smedialink.orangesales.activities.NewCompanyActivity;
import tim.smedialink.orangesales.activities.OfficeActivity;
import tim.smedialink.orangesales.adapters.RecyclerViewAdapter;
import tim.smedialink.orangesales.adapters.SimpleAdapter;
import tim.smedialink.orangesales.db.Office;
import tim.smedialink.orangesales.utils.LinearLayoutManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondRegPage extends Fragment {

    public ImageButton comLogo;
    public FloatingActionButton addOfficeBtn;

    public RecyclerView officeList;
    public RecyclerView pCatsList;

    public RecyclerViewAdapter adapter;
    public SimpleAdapter catsAdapter;

    public EditText comDescr;
    public EditText comAdditions;
    public EditText comSiteUrl;

    public SecondRegPage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_second_reg_page, container, false);

        comLogo = (ImageButton)rootView.findViewById(R.id.comLogo);
        comLogo.setOnClickListener(pickLogo);

        addOfficeBtn = (FloatingActionButton)rootView.findViewById(R.id.addOfficeBtn);
        addOfficeBtn.setOnClickListener(chooseOffice);

        officeList = (RecyclerView)rootView.findViewById(R.id.officesList);
        adapter = new RecyclerViewAdapter(getActivity(),((NewCompanyActivity)getActivity()).offices);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        LinearLayoutManager manager2 = new LinearLayoutManager(getContext());

        officeList.setLayoutManager(manager);
        officeList.setAdapter(adapter);

        pCatsList = (RecyclerView)rootView.findViewById(R.id.pCatsList);
        catsAdapter = new SimpleAdapter(((NewCompanyActivity)getActivity()).catNames);

        pCatsList.setLayoutManager(manager2);
        pCatsList.setAdapter(catsAdapter);

        rootView.findViewById(R.id.addCatBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivityForResult(new Intent(getActivity(), CheckCats.class), 1003);
            }
        });

        rootView.findViewById(R.id.regCompanyBtn)
                .setOnClickListener(((NewCompanyActivity) getActivity()).newCompanyClick);

        comDescr = (EditText)rootView.findViewById(R.id.e_comDescr);
        comAdditions = (EditText)rootView.findViewById(R.id.e_comAdditions);
        comSiteUrl = (EditText)rootView.findViewById(R.id.e_comSiteUrl);

        return rootView;
    }



    //------------CLICKERS-------------------//
    private View.OnClickListener pickLogo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_PICK);
            getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                    NewCompanyActivity.PICK_LOGO_CODE);
        }
    };

    private View.OnClickListener chooseOffice = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getContext(), OfficeActivity.class);
            getActivity().startActivityForResult(intent, 1002);
        }
    };


}

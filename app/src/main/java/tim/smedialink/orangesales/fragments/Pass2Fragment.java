package tim.smedialink.orangesales.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.ForgetPassword;
import tim.smedialink.orangesales.network.RestClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class Pass2Fragment extends Fragment {

    private ProgressDialog pd;
    private View view;

    public Pass2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pass2, container, false);
        view.findViewById(R.id.restorePassBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyCode(view);
            }
        });
        pd = new ProgressDialog(getContext());
        pd.setMessage("Подождите...");
        return view;
    }

    private void verifyCode(final View view) {
        pd.show();
        RestClient.getApi().verifyCode(((EditText)view.findViewById(R.id.editText2)).getText().toString(),
                new Callback<HashMap<String, String>>() {
            @Override
            public void success(HashMap<String, String> stringStringHashMap, Response response) {
                pd.dismiss();
                ForgetPassword.passwordToken = stringStringHashMap.get("password_reset_token");
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.pass_container, new Pass3Fragment())
                        .addToBackStack("pass")
                        .commit();
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                Snackbar.make(view, "Неверный код", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

}

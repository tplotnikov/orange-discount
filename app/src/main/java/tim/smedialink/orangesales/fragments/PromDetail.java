package tim.smedialink.orangesales.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.PartnerDetail;
import tim.smedialink.orangesales.activities.PromotionsActivity;
import tim.smedialink.orangesales.activities.VideoPlayerActivity;
import tim.smedialink.orangesales.adapters.AddrAdapter;
import tim.smedialink.orangesales.databinding.FragmentPromDetailBinding;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.CompanyModel;
import tim.smedialink.orangesales.models.MinPromotion;
import tim.smedialink.orangesales.models.PromotionModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.LinearLayoutManager;
import tim.smedialink.orangesales.utils.Settings;


public class PromDetail extends Fragment {

    private FragmentPromDetailBinding binding;
    private SliderLayout sliderShow;

    private RecyclerView officesList;
    private ImageView companyLogo;

    public static CompanyModel doNotCompany;

    public PromDetail() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_prom_detail, container, false);
        final View rootView = binding.getRoot();

        sliderShow = (SliderLayout)rootView.findViewById(R.id.promImage);
        sliderShow.setPresetIndicator(SliderLayout.PresetIndicators.Right_Bottom);
        companyLogo = (ImageView)rootView.findViewById(R.id.d_companyLogo);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setMessage("Подождите...");
        pd.show();

        PromotionsActivity activity = (PromotionsActivity)getActivity();
        RestClient.getApi().getOneProm(Cache.getAuth(), activity.getClickedPromotionId(),
                new Callback<PromotionModel>() {
                    @Override
                    public void success(final PromotionModel promotionModel, Response response) {
                        binding.setProm(promotionModel);
                        for (int i = 0; i < binding.getProm().getImages().size(); i++) {
                            DefaultSliderView imgView = new DefaultSliderView(getContext());
                            imgView.image(C.UPLOADS_BASE_URL + binding.getProm().getImages().get(i).resourceUri);
                            sliderShow.addSlider(imgView);
                        }
                        officesList = (RecyclerView) rootView.findViewById(R.id.d_companyOfficesList);
                        LinearLayoutManager manager = new LinearLayoutManager(getContext());
                        AddrAdapter addrAdapter = new AddrAdapter(getContext(), promotionModel.suboffices);
                        officesList.setLayoutManager(manager);
                        officesList.setAdapter(addrAdapter);
                        Picasso.with(getContext())
                                .load(C.UPLOADS_BASE_URL + promotionModel.getCompany().logoUrl)
                                .into(companyLogo);
                        setContacts(rootView);

                        doNotCompany = promotionModel.getCompany();

                        final RatingBar ratingBar = (RatingBar) rootView.findViewById(R.id.makrPartnerBar);
                        String alreadyRank = Settings.readData(getContext(), doNotCompany.id);

                        if (!alreadyRank.equals("")) {
                            ratingBar.setVisibility(View.GONE);
                            rootView.findViewById(R.id.textView69).setVisibility(View.VISIBLE);
                        }

                        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                            @Override
                            public void onRatingChanged(final RatingBar ratingBar, float rating, boolean fromUser) {
                                HashMap<String, String> erank = new HashMap<>();
                                erank.put("rank", String.valueOf(rating));
                                RestClient.getApi().setCRank(Cache.getAuth(), doNotCompany.getId(),
                                        erank, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {
                                                Settings.saveData(getContext(), doNotCompany.id, "1");
                                                ratingBar.setVisibility(View.GONE);
                                                rootView.findViewById(R.id.textView69).setVisibility(View.VISIBLE);
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {

                                            }
                                        });
                            }
                        });

                        if (promotionModel.videos != null && promotionModel.videos.size() > 0) {
                            TextView videoBtn = (TextView)rootView.findViewById(R.id.show_video_btn);
                            videoBtn.setVisibility(View.VISIBLE);
                            videoBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getActivity(), VideoPlayerActivity.class);
                                    intent.putExtra("video_source", promotionModel.videos.get(0).resourceUri);
                                    getActivity().startActivity(intent);
                                }
                            });
                        }

                        pd.dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        System.out.print(error.getMessage());
                        pd.dismiss();
                    }
                });

        rootView.findViewById(R.id.d_companyName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), PartnerDetail.class));
            }
        });


        return rootView;
    }

    private void setContacts(View view) {
        ((TextView)view.findViewById(R.id.d_email)).setText(binding.getProm().getCompany().getComContacts("email"));
        ((TextView)view.findViewById(R.id.d_siteUrl)).setText(binding.getProm().getCompany().getComContacts("web_site"));
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        sliderShow = null;
    }


}

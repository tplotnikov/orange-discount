package tim.smedialink.orangesales.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.BonusMarket;
import tim.smedialink.orangesales.activities.UserEditActivity;
import tim.smedialink.orangesales.databinding.FragmentUserCabinetBinding;
import tim.smedialink.orangesales.models.UserJ;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserCabinetFragment extends Fragment {

    private UserJ user;
    private ProgressDialog pd;
    private RelativeLayout fullUserProfile;
    private FragmentUserCabinetBinding binding;
    private TextView bonusExpireText;

    public UserCabinetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_user_cabinet, container, false);
        View view = binding.getRoot();
        pd = new ProgressDialog(getContext());
        pd.setMessage("Загрузка личных данных");
        pd.show();

        fullUserProfile = (RelativeLayout)view.findViewById(R.id.userFullProfile);
        loadUserProfile();

        bonusExpireText = (TextView)view.findViewById(R.id.cabUserBonusExpire);
        view.findViewById(R.id.cabUserBonusBtn).setOnClickListener(bonusClicker);

        view.findViewById(R.id.cabUserFullBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewGroup.LayoutParams lp = fullUserProfile.getLayoutParams();
                switch (lp.height) {
                    case 0:
                        lp.height = -2;
                        break;
                    case -2:
                        lp.height = 0;
                        break;
                }

                fullUserProfile.setLayoutParams(lp);
            }
        });

        view.findViewById(R.id.cabUserEditBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), UserEditActivity.class));
            }
        });

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        binding.setUser(Cache.getUser());
    }

    private void loadUserProfile() {
        RestClient.getApi().getProfile(Cache.getAuth(), new Callback<UserJ>() {
            @Override
            public void success(UserJ userJ, Response response) {
                user = userJ;
                binding.setUser(user);
                pd.dismiss();
                testTimer();
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
            }
        });
    }

    @SuppressWarnings("Depricated")
    private void testTimer() {

        if (user.bonus_expire == 0) {
            bonusExpireText.setTextSize(14f);
            bonusExpireText.setText("Вы ещё не пользовались картой");
        } else {

            Date now = Calendar.getInstance().getTime();
            Date exp = new Date(user.bonus_expire * 1000);

            long diff = now.getTime() - exp.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            bonusExpireText.setText(String.format("%s дней", Math.abs(days)));
        }

    }

    private View.OnClickListener bonusClicker = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getActivity(), BonusMarket.class));
        }
    };


}

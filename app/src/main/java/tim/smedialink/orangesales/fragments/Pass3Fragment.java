package tim.smedialink.orangesales.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.ForgetPassword;
import tim.smedialink.orangesales.network.RestClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class Pass3Fragment extends Fragment {


    private EditText editPass1;
    private EditText editPass2;
    private Button editPassBtn;

    private ProgressDialog pd;

    public Pass3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_pass3, container, false);
        editPass1 = (EditText)view.findViewById(R.id.editPass);
        editPass2 = (EditText)view.findViewById(R.id.editPass2);
        editPassBtn = (Button)view.findViewById(R.id.editPassBtn);
        pd = new ProgressDialog(getContext());
        pd.setMessage("Подождите...");
        editPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validatePassword(view);
            }
        });

        return view;
    }

    private void validatePassword(final View view) {
        pd.show();
        String pass1 = editPass1.getText().toString();
        String pass2 = editPass2.getText().toString();
        if (pass1.equals(pass2)) {
            RestClient.getApi().changePassword(ForgetPassword.passwordToken,
                    pass1, new Callback<Response>() {
                        @Override
                        public void success(Response response, Response response2) {
                            pd.dismiss();
                            getActivity().finish();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            pd.dismiss();
                            Snackbar.make(view, "Не удалось восстановить пароль", Snackbar.LENGTH_SHORT);
                        }
                    });
        } else {
            Snackbar.make(view, "Пароли не совпадают", Snackbar.LENGTH_SHORT).show();
        }
    }


}

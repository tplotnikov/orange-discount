package tim.smedialink.orangesales.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.activities.ForgetPassword;
import tim.smedialink.orangesales.activities.MainActivity;
import tim.smedialink.orangesales.db.DBManager;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.CompanyModel;
import tim.smedialink.orangesales.models.PartnerCompany;
import tim.smedialink.orangesales.network.OrangeCallback;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.Settings;
import tim.smedialink.orangesales.utils.SocialInit;
import tim.smedialink.orangesales.utils.Trash;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment
                            implements View.OnClickListener{

    private EditText editLogin;
    private EditText editLoginPassword;

    private ProgressDialog pd;
    private View mView;

    static public DBManager db;

    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_sign_in, container, false);

        editLogin = (EditText)mView.findViewById(R.id.editLogin);
        editLoginPassword = (EditText)mView.findViewById(R.id.editLoginPassword);

        Button loginButton = (Button) mView.findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);
        Button regButton = (Button) mView.findViewById(R.id.regButton);
        regButton.setOnClickListener(this);

        pd = new ProgressDialog(getContext());
        pd.setMessage("Загрузка...");
        //Если уже логинились или регались, то сразу переходим на главную
        db = new DBManager(getContext());

        mView.findViewById(R.id.forgotPassBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivityForResult(new Intent(getActivity(), ForgetPassword.class),
                        4001);
            }
        });

        getActivity().getSupportFragmentManager().beginTransaction()
                .add(R.id.socialContainer, new SocialInit())
                .commit();

        return mView;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regButton:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.sign_container, new SignUpFragment(), "regFragment")
                        .addToBackStack("login")
                        .commit();
                break;
            case R.id.loginButton:
                pd.show();
                checkLogin();
                break;
        }
    }

    private void checkLogin() {
        String login = editLogin.getText().toString();
        String password = editLoginPassword.getText().toString();

        RestClient.getApi().login(login, password, new Callback<OrangeCallback>() {
            @Override
            public void success(OrangeCallback orangeCallback, Response response) {
                Settings.saveData(getContext(), C.TOKEN, orangeCallback.access_token);
                Settings.saveData(getContext(), C.USERNAME, orangeCallback.username);
                Settings.saveData(getContext(), C.PASSWORD, editLoginPassword.getText().toString());
                Trash.saveUser(getContext(), orangeCallback.profile);
                Cache.setAuth(orangeCallback.access_token);
                Cache.setUser(orangeCallback.profile);
                checkPartner(orangeCallback.isPartner);
            }

            @Override
            public void failure(RetrofitError error) {
                editLoginPassword.setText("");
                pd.dismiss();
                Snackbar.make(editLogin, "Неверный логин/пароль", Snackbar.LENGTH_SHORT)
                        .show();
                Log.d("user", error.getMessage());
            }
        });
    }

    public void checkPartner(String isPartner) {
        String auth = "Bearer " + Settings.readData(getContext(), C.TOKEN);
        if (isPartner.equals("true")) {
            RestClient.getApi().getCompany(auth, new Callback<CompanyModel>() {
                @Override
                public void success(CompanyModel company, Response response) {
                    Cache.setCompany(company);
                    Gson gson = new GsonBuilder().create();
                    String json = gson.toJson(company);
                    Settings.saveData(getContext(), C.P_DATA, json);
                    pd.dismiss();
                    startActivity(new Intent(getContext(), MainActivity.class));
                    getActivity().finish();
                }
                @Override
                public void failure(RetrofitError error) {
                    pd.dismiss();
                    Snackbar.make(editLogin, "Неверный логин/пароль", Snackbar.LENGTH_SHORT)
                            .show();
                }
            });
        } else {
            pd.dismiss();
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        }
    }
}

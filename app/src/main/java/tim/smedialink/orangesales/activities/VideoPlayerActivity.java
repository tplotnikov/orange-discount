package tim.smedialink.orangesales.activities;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.VideoView;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.C;

public class VideoPlayerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        String videoSource = getIntent().getStringExtra("video_source");
        Log.d("video", C.UPLOADS_BASE_URL + videoSource);

        if (videoSource != null) {
            loadVideo(videoSource);
        }
    }

    private void loadVideo(String videoSource) {
//        VideoView videoView = (VideoView)findViewById(R.id.videoView);
//        videoView.setVideoURI(Uri.parse(C.UPLOADS_BASE_URL + videoSource));
//        videoView.requestFocus();
//        videoView.start();

        final MediaPlayer player = new MediaPlayer();
        try {
            player.setDataSource(this, Uri.parse(C.UPLOADS_BASE_URL + videoSource));
            player.prepareAsync();
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    player.start();
                }
            });

        } catch (Exception e) { e.printStackTrace(); }
    }
}

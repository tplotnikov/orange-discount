package tim.smedialink.orangesales.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.ChooserAdapter;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.CatsParser;
import tim.smedialink.orangesales.utils.ParamManager;

public class CheckCats extends AppCompatActivity {

    private ArrayList<String> ids = new ArrayList<>();
    private ArrayList<String> names = new ArrayList<>();
    ChooserAdapter adapter;
    ListView lvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_cats);

        lvMain = (ListView) findViewById(R.id.chooseCatsList);

        lvMain.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        switch (getIntent().getIntExtra("requestCode", 0)) {
            case SMSSend.ADD_AGES_CODE:
                HashMap<String, String> tempMap = new HashMap<>();
                String[] ages = getResources().getStringArray(R.array.ages);

                for (int i = 0; i < ages.length; i++) {
                    tempMap.put(String.valueOf(i), ages[i]);
                }
                adapter = new ChooserAdapter(this, tempMap);
                lvMain.setAdapter(adapter);
                break;
            case SMSSend.ADD_CITIES_CODE:
                getCities();
                break;
            case NewPromActivity.PROM_CATS_CODE:
                adapter = new ChooserAdapter(this, Cache.getCompany().getCompCatsWithIds());
                lvMain.setAdapter(adapter);
                break;
            case NewPromActivity.PROM_OFF_CODE:
                adapter = new ChooserAdapter(this, Cache.partnerOffices());
                lvMain.setAdapter(adapter);
                break;
            case NewCompanyActivity.CHOOSE_CATS_CODE:
                adapter = new ChooserAdapter(this, CatsParser.getSubCategoriesWithId());
                lvMain.setAdapter(adapter);
                break;
        }

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView ctv = (CheckedTextView)view.findViewById(R.id.pSubCatsList);
                adapter.toggle(ctv);
            }
        });


        findViewById(R.id.confirmCatsBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SparseBooleanArray chose = lvMain.getCheckedItemPositions();
                for (int i = 0; i < chose.size(); i++) {
                    int key = chose.keyAt(i);
                    if (chose.get(key)) {
                        names.add((String) adapter.getItem(key));
                        ids.add(adapter.getItemKey(key));
                    }
                }

                ParamManager.getInstance().setMapParams(ids);
                ParamManager.getInstance().setChosenCats(chose);

                Intent result = new Intent();
                result.putExtra("names", names);
                result.putExtra("ids", ids);
                setResult(RESULT_OK, result);
                finish();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
//        if (ParamManager.getInstance().getChosenCats() != null) {
//            SparseBooleanArray cats = ParamManager.getInstance().getChosenCats();
//            for (int i = 0; i < cats.size(); i++) {
//                int key = cats.keyAt(i);
//                lvMain.performItemClick(adapter.getView(key, null, null),
//                        key, adapter.getItemId(key));
//            }
//        }
    }

    private void getCities() {
        RestClient.getApi().getCities(Cache.getAuth(),
                new Callback<ArrayList<HashMap<String, String>>>() {
                    @Override
                    public void success(ArrayList<HashMap<String, String>> hashMaps, Response response) {
                        HashMap<String, String> cities = new HashMap<String, String>();
                        for(HashMap<String, String> temp: hashMaps) {
                            cities.put(temp.get("id"), temp.get("name"));
                        }

                        adapter = new ChooserAdapter(CheckCats.this, cities);
                        lvMain.setAdapter(adapter);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }
}

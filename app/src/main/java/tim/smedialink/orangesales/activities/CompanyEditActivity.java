package tim.smedialink.orangesales.activities;

import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.SimpleAdapter;
import tim.smedialink.orangesales.databinding.ActivityCompanyEditBinding;
import tim.smedialink.orangesales.databinding.ActivityUserEditBinding;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.PartnerCompany;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.CatsParser;
import tim.smedialink.orangesales.utils.LinearLayoutManager;
import tim.smedialink.orangesales.windows.MyAlert;

public class CompanyEditActivity extends AppCompatActivity {

    private ActivityCompanyEditBinding binding;

    private RecyclerView companyCats;
    private ArrayList<String> catIds = new ArrayList<>();
    private  ArrayList<String> cats = new ArrayList<>();

    private String pathImage;

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra("requestCode", requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != NewCompanyActivity.PICK_LOGO_CODE) {
            if (data != null && data.getStringArrayListExtra("names").size() > 0) {
                cats = data.getStringArrayListExtra("names");
                catIds = data.getStringArrayListExtra("ids");
                LinearLayoutManager manager = new LinearLayoutManager(this);
                SimpleAdapter adapter = new SimpleAdapter(cats);
                companyCats.setLayoutManager(manager);
                companyCats.setAdapter(adapter);
            }
        }else {
                try {
                    InputStream inputStream = getApplicationContext().getContentResolver().openInputStream(data.getData());
                    Bitmap logo = BitmapFactory.decodeStream(inputStream);
                    ((ImageView) findViewById(R.id.cabPLogo)).setImageBitmap(logo);

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    pathImage = cursor.getString(columnIndex);
                    cursor.close();
                } catch (FileNotFoundException | NullPointerException e) {
                    e.printStackTrace();
                }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_company_edit);
        binding.setCompany(Cache.getCompany());

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Редактирование профиля");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        companyCats = (RecyclerView)findViewById(R.id.cabCatsList);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        SimpleAdapter adapter = new SimpleAdapter(Cache.getCompany().getCompCats());
        companyCats.setLayoutManager(manager);
        companyCats.setAdapter(adapter);
        setContacts();

        ImageView logo = (ImageView)findViewById(R.id.cabPLogo);
        if(binding.getCompany().logoUrl != null) {
            Picasso.with(this)
                    .load(C.UPLOADS_BASE_URL + binding.getCompany().logoUrl)
                    .into(logo);
        }
        logo.setOnClickListener(pickLogo);
        FloatingActionButton editCatBtn = (FloatingActionButton)findViewById(R.id.addCatsFBtn);
        editCatBtn.setOnClickListener(addCats);

        findViewById(R.id.saveEditedBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });
    }

    private View.OnClickListener pickLogo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                    NewCompanyActivity.PICK_LOGO_CODE);
        }
    };

    private void setContacts() {
        ((EditText)findViewById(R.id.cabEmail)).setText(binding.getCompany().getComContacts("email"));
        ((EditText)findViewById(R.id.cabSPhone)).setText(binding.getCompany().getComContacts("work_phone"));
        ((EditText)findViewById(R.id.cabMPhone)).setText(binding.getCompany().getComContacts("mobile_phone"));
    }

    private View.OnClickListener addCats = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivityForResult(new Intent(CompanyEditActivity.this, CheckCats.class), 1003);
        }
    };

    private void updateData() {
        HashMap<String, String> edited = new HashMap<>();
        ArrayList<PartnerCompany.Contact> contacts = new ArrayList<>();
        edited.put("name", ((EditText)findViewById(R.id.cabName)).getText().toString());
        edited.put("legal_name", ((EditText)findViewById(R.id.cabLegalName)).getText().toString());
        edited.put("inn", ((EditText) findViewById(R.id.cabInn)).getText().toString());
        edited.put("legal_address", ((EditText) findViewById(R.id.cabAddr)).getText().toString());
        edited.put("description", ((EditText) findViewById(R.id.cabDescr)).getText().toString());
        contacts.add(new PartnerCompany.Contact("mobile_phone", ((EditText) findViewById(R.id.cabMPhone)).getText().toString()));
        contacts.add(new PartnerCompany.Contact("work_phone", ((EditText) findViewById(R.id.cabSPhone)).getText().toString()));
        contacts.add(new PartnerCompany.Contact("email", ((EditText) findViewById(R.id.cabEmail)).getText().toString()));
        HashMap<String, ArrayList<PartnerCompany.Contact>> data = new HashMap<>();
        data.put("contacts", contacts);
        HashMap<String, ArrayList<String>> categories = new HashMap<>();
        categories.put("categories", catIds);

        RestClient.getApi().updateCompany(Cache.getAuth(), edited,
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        MyAlert.getInstance().showMessage(CompanyEditActivity.this, "Заявка на обновление данных партнера отправлена");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        MyAlert.getInstance().showMessage(CompanyEditActivity.this, "Произошла ошибка");
                    }
                });

        RestClient.getApi().renewContacts(Cache.getAuth(), data,
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });

        RestClient.getApi().addCat(Cache.getAuth(), categories, new Callback<HashMap<String, String>>() {
            @Override
            public void success(HashMap<String, String> stringStringHashMap, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

        if (pathImage != null) {
            RestClient.getApi().setLogo(Cache.getAuth(), binding.getCompany().getId(), new TypedFile("image/*", new File(pathImage)),
                    new Callback<Response>() {
                        @Override
                        public void success(Response response, Response response2) {

                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
        }
    }

}

package tim.smedialink.orangesales.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.databinding.ActivityOfficeBinding;
import tim.smedialink.orangesales.geo.CityAdapter;
import tim.smedialink.orangesales.geo.Tracker;
import tim.smedialink.orangesales.models.OfficeModel;

public class OfficeActivity extends AppCompatActivity implements
        OnMapReadyCallback {

    private GoogleMap mMap;
    private Geocoder mGeocoder;
    private OfficeModel office;
    Marker officeMarker;
    private OfficeHandler handler;
    private Tracker tracker;
    ActivityOfficeBinding binding;

    EditText detailStart;
    EditText detailEnd;
    Button confirmBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_office);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_office);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Новый филиал");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);

        office = new OfficeModel();
        binding.setOffice(office);
        mGeocoder = new Geocoder(this);
        handler = new OfficeHandler();
        tracker = new Tracker(this);

        findViewById(R.id.view3).setOnClickListener(onChooseClick);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tracker.clean();
        tracker = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        handler.map = mMap;

        officeMarker = mMap.addMarker(
                new MarkerOptions().draggable(true)
                .position(new LatLng(0, 0))
        );

        mMap.setOnMarkerDragListener(dragListener);
        mMap.setOnMapClickListener(mapClick);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_aaaa, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager)OfficeActivity.this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(onSearchChange);

        return super.onCreateOptionsMenu(menu);
    }

    private void getAddress(final double lat,
                            final double lon,
                            @Nullable final String name,
                            final Handler handler) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                String result = null;
                String locale = null;
                double latitude = 0;
                double longitude = 0;
                List<Address> addresses;
                try {
                    if(name == null) {
                        addresses = geocoder.getFromLocation(lat, lon, 1);
                    } else {
                        addresses = geocoder.getFromLocationName(name, 1);
                    }
                    if(addresses.size() > 0) {
                        locale = addresses.get(0).getLocality();
                        result = addresses.get(0).getAddressLine(0);
                        latitude = addresses.get(0).getLatitude();
                        longitude =addresses.get(0).getLongitude();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 202;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        bundle.putString("city", locale);
                        bundle.putDouble("lat", latitude);
                        bundle.putDouble("lon", longitude);
                        message.setData(bundle);
                    } else {
                        message.what = 202;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", "");
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        });
        thread.start();
    }


    GoogleMap.OnMarkerDragListener dragListener = new GoogleMap.OnMarkerDragListener() {
        @Override
        public void onMarkerDragStart(Marker marker) {

        }

        @Override
        public void onMarkerDrag(Marker marker) {

        }

        @Override
        public void onMarkerDragEnd(Marker marker) {
            getAddress(marker.getPosition().latitude,
                    marker.getPosition().longitude, null, handler);
        }
    };

    GoogleMap.OnMapClickListener mapClick = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            getAddress(latLng.latitude, latLng.longitude, null, handler);
            officeMarker.setPosition(latLng);
        }
    };

    SearchView.OnQueryTextListener onSearchChange = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    getAddress(0, 0, query, handler);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
    };

    View.OnClickListener onChooseClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!binding.getOffice().getAddress().equals("")) {
                officeDetailsDialog().show();
            }
        }
    };

    /*----------------------------------------------------------------------
      ----------------------------------------------------------------------
      ----------------------------------------------------------------------
      ----------------------- Part of dialog window ------------------------
     */


    TimePickerDialog.OnTimeSetListener onTimeStart = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            String min = String.valueOf(minute);
            if(min.length() < 2) min = String.format("0%s", min);
            detailStart.setText(String.format("%s:%s", hourOfDay, min));
        }
    };

    TimePickerDialog.OnTimeSetListener onEndTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            detailEnd.setText(String.format("%s:%s", hourOfDay, minute));
        }
    };

    private View.OnFocusChangeListener time = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(final View view, boolean b) {
            if (view.isFocused()) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = null;
                switch (view.getId()) {
                    case R.id.oDetailStartTime:
                        tpd = TimePickerDialog.newInstance(
                                onTimeStart, now.get(Calendar.HOUR_OF_DAY),
                                now.get(Calendar.MINUTE), true);
                        break;
                    case R.id.oDetailEndTime:
                        tpd = TimePickerDialog.newInstance(
                                onEndTime, now.get(Calendar.HOUR_OF_DAY),
                                now.get(Calendar.MINUTE), true);
                        break;
                }
                tpd.show(getFragmentManager(), "s");
                view.clearFocus();
            }
        }
    };

    private AlertDialog officeDetailsDialog() {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(OfficeActivity.this);
        AwesomeValidation validation = new AwesomeValidation(ValidationStyle.UNDERLABEL);
        validation.setContext(OfficeActivity.this);


        View view = LayoutInflater.from(OfficeActivity.this).inflate(
                R.layout.office_dialog, null, false);
        alertDialog.setView(view).setTitle("Дополнительная информация");

        final AlertDialog dialog = alertDialog.create();
        final EditText detailPhone = (EditText)view.findViewById(R.id.oDetailPhone);
        detailPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        validation.addValidation(detailPhone, "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$", getString(R.string.phone_error));

        detailStart = (EditText)view.findViewById(R.id.oDetailStartTime);
        detailStart.setOnFocusChangeListener(time);
        detailEnd = (EditText)view.findViewById(R.id.oDetailEndTime);
        detailEnd.setOnFocusChangeListener(time);
        confirmBtn = (Button)view.findViewById(R.id.confirmOfficeBtn);
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                binding.getOffice().setPhone(detailPhone.getText().toString().replaceAll("\\D", ""));
                binding.getOffice().setStart_time(detailStart.getText().toString());
                binding.getOffice().setEnd_time(detailEnd.getText().toString());
                Intent intent = new Intent();
                intent.putExtra("latitude", binding.getOffice().getLatitude());
                intent.putExtra("longitude", binding.getOffice().getLongitude());
                intent.putExtra("address", binding.getOffice().getAddress());
                intent.putExtra("city", binding.getOffice().getCity());
                intent.putExtra("phone", binding.getOffice().getPhone());
                intent.putExtra("start_time", binding.getOffice().getStart_time());
                intent.putExtra("end_time", binding.getOffice().getEnd_time());
                setResult(RESULT_OK, intent);
                dialog.dismiss();
                finish();
            }
        });


        return dialog;
    }
/* ---------------------------------------------------------------------------- */
    private class OfficeHandler extends Handler {

        public GoogleMap map;
        public String address;

        private double latitude;
        private double longitude;

        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            latitude = bundle.getDouble("lat");
            longitude = bundle.getDouble("lon");
            LatLng position = new LatLng(latitude, longitude);

            address = bundle.getString("address");
            if (map != null) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 17.0f));

                officeMarker.setPosition(position);
                binding.getOffice().setAddress(address);
                binding.getOffice().setLatitude(position.latitude);
                binding.getOffice().setLongitude(longitude);
                binding.getOffice().setCity(bundle.getString("city"));
            }
        }
    }
}

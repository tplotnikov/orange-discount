package tim.smedialink.orangesales.activities;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.fragments.PromDetail;
import tim.smedialink.orangesales.fragments.PromListFragment;
import tim.smedialink.orangesales.models.MinPromotion;
import tim.smedialink.orangesales.models.PromotionModel;

public class PromotionsActivity extends AppCompatActivity {

    public String subCatId;
    private String clickedPromotionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotions);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra("title"));
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        subCatId = getIntent().getStringExtra("subCatId");

        if (getIntent().getStringExtra("flag") == null) {
            PromListFragment promsListFragment = PromListFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.proms_container, promsListFragment, "PROMS")
                    .commit();
        } else {
            choosePromotion(getIntent().getStringExtra("flag"), true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void choosePromotion(String modelId, boolean flag) {

        this.clickedPromotionId = modelId;

        if (flag) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.proms_container, new PromDetail())
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.proms_container, new PromDetail())
                    .addToBackStack("proms")
                    .commit();
        }
    }

    public String getClickedPromotionId() {
        return clickedPromotionId;
    }
}

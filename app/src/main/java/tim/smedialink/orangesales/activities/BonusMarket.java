package tim.smedialink.orangesales.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.BonusListAdapter;
import tim.smedialink.orangesales.models.BonusModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.NoInternetAlert;

public class BonusMarket extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bonus_market);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.bonus_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getMarket();
    }

    private void getMarket() {
        final RecyclerView bonusList = (RecyclerView)findViewById(R.id.market_list);

        RestClient.getApi().getLotteries(Cache.getAuth(),
                new Callback<ArrayList<BonusModel>>() {
                    @Override
                    public void success(ArrayList<BonusModel> bonusModel, Response response) {
                        BonusListAdapter adapter = new BonusListAdapter(BonusMarket.this,
                                bonusModel);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(BonusMarket.this,
                                android.support.v7.widget.LinearLayoutManager.VERTICAL, false);
                        bonusList.setLayoutManager(layoutManager);
                        bonusList.setAdapter(adapter);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        new NoInternetAlert(BonusMarket.this);
                    }
                });
    }
}

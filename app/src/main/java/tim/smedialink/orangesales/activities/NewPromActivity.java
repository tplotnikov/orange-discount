package tim.smedialink.orangesales.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationHolder;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.validators.Validator;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.SimpleAdapter;
import tim.smedialink.orangesales.models.CabPromModel;
import tim.smedialink.orangesales.models.PriceModel;
import tim.smedialink.orangesales.models.PromotionModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.LinearLayoutManager;
import tim.smedialink.orangesales.utils.MediaLoader;
import tim.smedialink.orangesales.utils.PriceCounter;
import tim.smedialink.orangesales.utils.RealPathUtil;
import tim.smedialink.orangesales.windows.MyAlert;

public class NewPromActivity extends AppCompatActivity {

    public static final int PROM_CATS_CODE = 4001;
    public static final int PROM_OFF_CODE = 4002;
    public static final int PROM_MEDIA_CODE = 4003;

    PriceData pd;

    TabLayout moneyTabs;
    TabLayout tabs;
    TabLayout.Tab tab1;
    TabLayout.Tab tab3;
    TabLayout.Tab tab6;

    private TextView planDescr;
    private TextView paySumm;
    private CardView mediaCard;

    private Button promSubmitBtn;

    private ListView payItemsList;

    private HashMap<String, Object> userInput;
    private ArrayList<String> catIds = new ArrayList<>();
    private ArrayList<String> officeIds = new ArrayList<>();

    private String currentPlan;
    private ImageButton pressedButton;
    private Uri selectedImage;

    MediaLoader loader;
    AwesomeValidation validator;

    @Override
    public void startActivityForResult(Intent intent, int requestCode){
        intent.putExtra("requestCode", requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(intent != null) {
            switch (requestCode) {
                case PROM_CATS_CODE:
                    RecyclerView newPromCatsList = (RecyclerView) findViewById(R.id.newPromCatsList);
                    LinearLayoutManager llManager = new LinearLayoutManager(this);
                    SimpleAdapter adapter = new SimpleAdapter(intent.getStringArrayListExtra("names"));
                    newPromCatsList.setLayoutManager(llManager);
                    newPromCatsList.setAdapter(adapter);
                    catIds = intent.getStringArrayListExtra("ids");
                    break;
                case PROM_OFF_CODE:
                    RecyclerView officeList = (RecyclerView) findViewById(R.id.officeList);
                    LinearLayoutManager ll2Manager = new LinearLayoutManager(this);
                    SimpleAdapter adapter2 = new SimpleAdapter(intent.getStringArrayListExtra("names"));
                    officeList.setLayoutManager(ll2Manager);
                    officeList.setAdapter(adapter2);
                    officeIds = intent.getStringArrayListExtra("ids");
                    break;
                case PROM_MEDIA_CODE:
                    try {
                        selectedImage = intent.getData();
                        InputStream is = this.getContentResolver().openInputStream(intent.getData());
                        Bitmap image = BitmapFactory.decodeStream(is);
                        if (image != null) {
                            pressedButton.setImageBitmap(image);
                            loader.addImagePath(RealPathUtil.getImagePath(this, intent.getData()));
                        } else {
                            pressedButton.setImageDrawable(getResources().getDrawable(R.drawable.placeholder_video));
                            loader.addVideoPath(RealPathUtil.getVideoPath(this, intent.getData()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }

            PriceCounter.getInstance().calculate(currentPlan, catIds.size(), officeIds.size());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_prom);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.new_promotion_btn);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.promDateStart).setOnFocusChangeListener(date);
        findViewById(R.id.promDateEnd).setOnFocusChangeListener(date);

        planDescr = (TextView)findViewById(R.id.planDescr);
        mediaCard = (CardView)findViewById(R.id.mediaCard);
        paySumm = (TextView)findViewById(R.id.paySumm);
        makeTabs();

        FloatingActionButton addCatBtn = (FloatingActionButton)findViewById(R.id.promAddCatBtn);
        addCatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(NewPromActivity.this, CheckCats.class), PROM_CATS_CODE);
            }
        });

        FloatingActionButton addOfficeBtn = (FloatingActionButton)findViewById(R.id.promAddOfficeBtn);
        addOfficeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(NewPromActivity.this, CheckCats.class), PROM_OFF_CODE);
            }
        });

        promSubmitBtn = (Button)findViewById(R.id.promSubmitBtn);
        promSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fillUserInput();
                if (validator.validate()) {
                    createPromotion();
                }
            }
        });

        findViewById(R.id.promPolicyCheck).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promSubmitBtn.setEnabled(((CheckBox) v).isChecked());
            }
        });

        findViewById(R.id.mediaBtn1).setOnClickListener(mediaListener);
        findViewById(R.id.mediaBtn2).setOnClickListener(mediaListener);
        findViewById(R.id.mediaBtn3).setOnClickListener(mediaListener);
        findViewById(R.id.mediaBtn4).setOnClickListener(mediaListener);
        pd = new PriceData();
        getPrices();

        ((TextView)findViewById(R.id.textView67)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewPromActivity.this, EULAActivity.class);
                intent.putExtra("flag", 2);
                startActivity(intent);
            }
        });

        loader = new MediaLoader();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (tabs.getSelectedTabPosition() == 1)
            mediaCard.setVisibility(View.VISIBLE);

        validator = new AwesomeValidation(ValidationStyle.BASIC);
        validateUserInput();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (tabs.getSelectedTabPosition() == 1)
            mediaCard.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener mediaListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            pressedButton = (ImageButton)v;
            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/* video/*");
            startActivityForResult(pickIntent, PROM_MEDIA_CODE);
        }
    };
    DatePickerDialog.OnDateSetListener dateStart = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
            ((EditText)findViewById(R.id.promDateStart)).setText(date);
        }
    };

    DatePickerDialog.OnDateSetListener dateEnd = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
            ((EditText)findViewById(R.id.promDateEnd)).setText(date);
        }
    };

    private View.OnFocusChangeListener date = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(final View view, boolean b) {
            if (view.isFocused()) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = null;
                switch (view.getId()) {
                    case R.id.promDateStart:
                        dpd = DatePickerDialog.newInstance(
                            dateStart,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                        );
                        break;
                    case R.id.promDateEnd:
                        dpd = DatePickerDialog.newInstance(
                                dateEnd,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );
                        break;
                }
                dpd.show(getFragmentManager(), "s");
            }
        }
    };

    private void makeTabs() {

        //-------------------------------------------------------------------------------
        moneyTabs = (TabLayout)findViewById(R.id.moneyTabs);
        tab1 = moneyTabs.newTab()
                .setText("1 месяц");
        tab3 = moneyTabs.newTab()
                .setText("3 месяца");
        tab6 = moneyTabs.newTab()
                .setText("6 месяцев");

        moneyTabs.addTab(tab1);
        moneyTabs.addTab(tab3);
        moneyTabs.addTab(tab6);

        moneyTabs.setSelectedTabIndicatorHeight(100);
        moneyTabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.inditabs));
        moneyTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tabs.getSelectedTabPosition() == 0) {
                    setStandart();
                } else {
                    setPremium();
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    //-------------------------------------------------------------------------------
        tabs = (TabLayout)findViewById(R.id.tabs);

        TabLayout.Tab standartTab = tabs.newTab()
                .setText("Стандарт");
        TabLayout.Tab premiumTab = tabs.newTab()
                .setText("Премиум");

        tabs.addTab(standartTab);
        tabs.addTab(premiumTab);

        tabs.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tabs.setTabTextColors(getResources().getColor(R.color.et_divider_not_focused),
                getResources().getColor(R.color.material_drawer_dark_primary_text));
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        setStandart();
                        break;
                    case 1:
                        setPremium();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        setStandart();
                        break;
                    case 1:
                        setPremium();
                        break;
                }
            }
        });
    }

    private void setPremium() {
        planDescr.setText(pd.premiumPlanDescription);
        tab1.setText(String.format("1 month\n%s", pd.month1PremiumPrice));
        tab3.setText(String.format("3 month\n%s", pd.month3PremiumPrice));
        tab6.setText(String.format("6 month\n%s", pd.month6PremiumPrice));
        switch (moneyTabs.getSelectedTabPosition()) {
            case 0:
                currentPlan = "premium_1month";
                break;
            case 1:
                currentPlan = "premium_3month";
                break;
            case 2:
                currentPlan = "premium_6month";
                break;
        }
        mediaCard.setVisibility(View.VISIBLE);
        PriceCounter.getInstance().calculate(currentPlan, catIds.size(), officeIds.size());
    }

    private void setStandart() {
        planDescr.setText(pd.standartPlanDescription);
        tab1.setText(String.format("1 Месяц\n%s", pd.month1StandartPrice));
        tab3.setText(String.format("3 Месяца\n%s", pd.month3StandartPrice));
        tab6.setText(String.format("6 Месяцев\n%s", pd.month6StandartPrice));

        switch (moneyTabs.getSelectedTabPosition()) {
            case 0:
                currentPlan = "standard_1month";
                break;
            case 1:
                currentPlan = "standard_3month";
                break;
            case 2:
                currentPlan = "standard_6month";
                break;
        }
        mediaCard.setVisibility(View.GONE);
        PriceCounter.getInstance().calculate(currentPlan, catIds.size(), officeIds.size());
    }

    private void fillUserInput() {
        userInput = new HashMap<>();

        userInput.put("title", ((EditText) findViewById(R.id.promName)).getText().toString());
        userInput.put("description", ((EditText) findViewById(R.id.promDescr)).getText().toString());
        userInput.put("planId", currentPlan);
        userInput.put("start_date", ((EditText) findViewById(R.id.promDateStart)).getText().toString());
        userInput.put("end_date", ((EditText) findViewById(R.id.promDateEnd)).getText().toString());
        userInput.put("cats", catIds);
        userInput.put("addresses", officeIds);
        userInput.put("discount_value", ((EditText) findViewById(R.id.promDiscountValue)).getText().toString());
    }

    private void validateUserInput() {
        validator.addValidation(this, R.id.promName, ".{1,}", R.string.empty_error);
        validator.addValidation(this, R.id.promDescr, ".{1,}", R.string.empty_error);
        validator.addValidation(this, R.id.promDiscountValue, ".{1,}", R.string.empty_error);
    }
    
    private class PriceData {
        //TODO: Заменить на данные с сервера
        
        public String standartPlanDescription = getString(R.string.standart_descr);
        public String premiumPlanDescription = getString(R.string.premium_descr);

        public String month1StandartPrice = "100 рублей";
        public String month3StandartPrice = "300 рублей";
        public String month6StandartPrice = "500 рублей";

        public String month1PremiumPrice = "200 рублей";
        public String month3PremiumPrice = "400 рублей";
        public String month6PremiumPrice = "600 рублей";
    }

    private void getPrices() {
        RestClient.getApi().getPrices(Cache.getAuth(),
                new Callback<ArrayList<PriceModel>>() {
                    @Override
                    public void success(ArrayList<PriceModel> priceModel, Response response) {
                        HashMap<String, PriceModel> pricesMap = new HashMap<>();
                        for (int i = 0; i < priceModel.size(); i++) {
                            pricesMap.put(priceModel.get(i).getId(), priceModel.get(i));
                        }
                        PriceCounter.getInstance().setPrices(pricesMap, paySumm);
                        pd = new PriceData();
                        pd.month1PremiumPrice = String.valueOf(pricesMap.get("premium_1month").getPrice());
                        pd.month3PremiumPrice = String.valueOf(pricesMap.get("premium_3month").getPrice());
                        pd.month6PremiumPrice = String.valueOf(pricesMap.get("premium_6month").getPrice());
                        pd.month1StandartPrice = String.valueOf(pricesMap.get("standard_1month").getPrice());
                        pd.month3StandartPrice = String.valueOf(pricesMap.get("standard_3month").getPrice());
                        pd.month6StandartPrice = String.valueOf(pricesMap.get("standard_6month").getPrice());
                        currentPlan = "standard_1month";
                        setStandart();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                    }
                });
    }

    private void createPromotion() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Подождите...");
        pd.show();

        RestClient.getApi().createPromotion(Cache.getAuth(),
                userInput, new Callback<CabPromModel>() {
                    @Override
                    public void success(CabPromModel response, Response response2) {
                        final String id = response.id;
                        Cache.getCompany().promotions.add(response);
                        addRes(id, pd);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (error.toString().contains("java.io.EOFException")) {
                            createPromotion();
                        } else {
                            MyAlert.getInstance().showMessage(NewPromActivity.this, "Не удалось создать акцию.");
                            pd.dismiss();
                        }
                        Log.d("prom_create", error.getMessage());
                    }
                });
    }

    private void addRes(final String id, final ProgressDialog progressDialog) {
        final HashMap<String, TypedFile> imgs = new HashMap<>();
        final HashMap<String, TypedFile> vids = new HashMap<>();
        int imgSize = loader.getImageCount();
        int vidSize = loader.getVideoCount();

        if (imgSize > 0) {
            for (int i = 0; i < imgSize; i++) {
                imgs.put("images[" + i + "]", loader.makeTypedImages().get(i));
            }
        }

        if (vidSize >0 ) {
            for (int i = 0; i < vidSize; i++) {
                imgs.put("videos[" + i + "]", loader.makeTypedVideos().get(i));
            }
        }
        RestClient.getApi().addResources(Cache.getAuth(), id,
                imgs, vids, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        progressDialog.dismiss();
                        showSuccess(id);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressDialog.dismiss();
                    }
                });
    }

    private void showSuccess(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(NewPromActivity.this)
                .setMessage("Акция успешно создана!\nВы можете вернуться в личный кабинет или " +
                        "перейти на детальное представление акции.")
                .setNegativeButton("Личный кабинет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setPositiveButton("Детальное представление", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(NewPromActivity.this, PromotionsActivity.class);
                        intent.putExtra("flag", id);
                        startActivity(intent);
                        finish();
                    }
                });
        builder.create().show();
    }
}

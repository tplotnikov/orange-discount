package tim.smedialink.orangesales.activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.QR;
import tim.smedialink.orangesales.utils.Settings;

public class CodeActivity extends AppCompatActivity {

    private TextView codeCardNum;
    private ImageView codeImage;

    String userCardNumber;

    private BarcodeFormat format;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);

        final String code = getIntent().getStringExtra("format");
        userCardNumber = Cache.getUser().username;

        switch (code) {
            case "QR":
                format = BarcodeFormat.QR_CODE;
                break;
            case "bar":
                format = BarcodeFormat.EAN_13;
                userCardNumber = "2700000007778";
                break;
        }

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(String.format("Мой %s-код", code));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CodeActivity.this.onBackPressed();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        codeCardNum = (TextView)findViewById(R.id.codeCardNum);
        codeCardNum.setText(userCardNumber);
        codeImage = (ImageView)findViewById(R.id.codeImage);

        generateQR(format);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void generateQR(BarcodeFormat mFormat) {

        try {
            Bitmap bitmap = QR.encodeAsBitmap(userCardNumber, mFormat, 380, 380);
            codeImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

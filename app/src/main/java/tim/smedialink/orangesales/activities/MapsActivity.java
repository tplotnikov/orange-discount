package tim.smedialink.orangesales.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.databinding.MapPromItemBinding;
import tim.smedialink.orangesales.databinding.PromItemBinding;
import tim.smedialink.orangesales.geo.NearPromotionMarker;
import tim.smedialink.orangesales.geo.OwnIconRenderer;
import tim.smedialink.orangesales.geo.Tracker;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.MinPromotion;
import tim.smedialink.orangesales.models.OfficeModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.MyHandlers;
import tim.smedialink.orangesales.utils.NoInternetAlert;
import tim.smedialink.orangesales.utils.ParamManager;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback  {

    private GoogleMap mMap;
    private Tracker tracker;
    private ClusterManager<NearPromotionMarker> clusterManager;
    private ArrayList<MinPromotion> fuckingProms;

    private Location targetLocation;
    private ImageButton directionBtn;

    private HashMap<String, LatLng> promsPositions = new HashMap<>();

    MapPromItemBinding binding;

    private double addrLat;
    private double addrLon;
    private String addr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.nav_map_label);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (getIntent().getDoubleExtra("lat",0) != 0) {
            addrLat = getIntent().getDoubleExtra("lat", 0);
            addrLon = getIntent().getDoubleExtra("lon", 0);
            mapFragment.getMapAsync(mapReadyCallback);
        } else {
            mapFragment.getMapAsync(this);
            tracker = new Tracker(this);
            directionBtn = (ImageButton)findViewById(R.id.directionBtn);
            binding = DataBindingUtil.bind(findViewById(R.id.infoWindow));
        }
        findViewById(R.id.infoWindow).setVisibility(View.INVISIBLE);

    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra("requestCode", requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ArrayList<String> params = ParamManager.getInstance().getMapParams();
        RestClient.getApi().getNearProms(Cache.getAuth(), params,
                new Callback<ArrayList<MinPromotion>>() {
                    @Override
                    public void success(ArrayList<MinPromotion> minPromotions, Response response) {
                        fuckingProms = (ArrayList<MinPromotion>) minPromotions.clone();
                        fillClusters();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                    }
                });
    }

    OnMapReadyCallback mapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            googleMap.addMarker(
                    new MarkerOptions()
                            .position(new LatLng(addrLat, addrLon)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(addrLat, addrLon),17 ));
        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        clusterManager = new ClusterManager<>(this, mMap);
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraChangeListener(clusterManager);
        mMap.setOnMarkerClickListener(clusterManager);

        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                findViewById(R.id.infoWindow).setVisibility(View.INVISIBLE);
            }
        });
        tracker.getCityCoordinates(new CoorHandler(), null);
        clusterManager.setRenderer(new OwnIconRenderer(this, mMap, clusterManager));
        clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<NearPromotionMarker>() {
            @Override
            public boolean onClusterItemClick(final NearPromotionMarker nearPromotionMarker) {
                final MinPromotion promotion = nearPromotionMarker.getPromotion();
                MyHandlers handlers = new MyHandlers();
                handlers.id = promotion.getId();
                binding.setPromotion(promotion);
                binding.setHandlers(handlers);
                String promLogoUrl;
                if(!promotion.getImages().isEmpty()) {
                    promLogoUrl = C.UPLOADS_BASE_URL+promotion.getImages().get(0).resourceUri;
                    Picasso.with(MapsActivity.this)
                            .load(promLogoUrl)
                            .into((ImageView)findViewById(R.id.infoWindow).findViewById(R.id.pl_Logo));
                }
                targetLocation = new Location("");//provider name is unecessary
                targetLocation.setLatitude(nearPromotionMarker.getPosition().latitude);//your coords of course
                targetLocation.setLongitude(nearPromotionMarker.getPosition().longitude);
                directionBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String q = promotion.getSuboffices().get(0).getAddress();
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + q);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    }
                });

                findViewById(R.id.infoWindow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MapsActivity.this, PromotionsActivity.class);
                        intent.putExtra("flag", promotion.getId());
                        startActivity(intent);
                    }
                });

                float distanceInMeters =  targetLocation.distanceTo(mMap.getMyLocation());
                String distanceString = String.valueOf(Float.valueOf(distanceInMeters).intValue());
                ((TextView) findViewById(R.id.metres)).setText(distanceString+" метров");
                findViewById(R.id.infoWindow).setVisibility(View.VISIBLE);
                return false;
            }
        });
        getMapPromShit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_map, menu);
        MenuItem searchItem = menu.findItem(R.id.action_filter);

        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(MapsActivity.this, CheckCats.class);
                startActivityForResult(intent, 1003);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void getMapPromShit() {
        RestClient.getApi().getNearProms(Cache.getAuth(), null,
                new Callback<ArrayList<MinPromotion>>() {
                    @Override
                    public void success(ArrayList<MinPromotion> minPromotions, Response response) {
                        fuckingProms = (ArrayList<MinPromotion>)minPromotions.clone();
                        fillClusters();
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    private void fillClusters() {
        clusterManager.clearItems();
        for (int i = 0; i < fuckingProms.size(); i++) {
            MinPromotion prom = fuckingProms.get(i);
            for (OfficeModel office : prom.getSuboffices()) {
                LatLng position = new LatLng(office.getLatitude(), office.getLongitude());
                NearPromotionMarker marker = new NearPromotionMarker(position, prom);
                promsPositions.put(office.id, position);
                clusterManager.addItem(marker);
            }
        }
        clusterManager.cluster();
    }




    class CoorHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            double latitude = message.getData().getDouble("lat");
            double longitude = message.getData().getDouble("lon");
            LatLng myPos = new LatLng(latitude, longitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPos, 13.0f));
            tracker.stopUsingGps();
        }
    }
}

package tim.smedialink.orangesales.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import tim.smedialink.orangesales.R;

public class EULAActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eula);

        WebView webView = (WebView)findViewById(R.id.webView);

        int flag = getIntent().getIntExtra("flag", 0);
        if (flag > 0) {
            switch (flag) {
                case 1:
                    webView.loadUrl("http://orangesales.tk/orange/web/documents/eula.txt");
                    break;
                case 2:
                    webView.loadUrl("http://orangesales.tk/orange/web/documents/eula_partner.txt");
                    break;
            }
        }
    }
}

package tim.smedialink.orangesales.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.billing.IabHelper;
import tim.smedialink.orangesales.fragments.CabinetFragment;
import tim.smedialink.orangesales.fragments.CategoriesFragment;
import tim.smedialink.orangesales.fragments.FavoritesFragment;
import tim.smedialink.orangesales.fragments.UserCabinetFragment;
import tim.smedialink.orangesales.gcm.RegistrationIntentService;
import tim.smedialink.orangesales.models.OfficeModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.PushService;
import tim.smedialink.orangesales.utils.Settings;
import tim.smedialink.orangesales.windows.CustomPrimItem;
import tim.smedialink.orangesales.windows.MyAlert;

public class MainActivity extends AppCompatActivity {

    private int selection;
    private int startSelection;
    public static Drawer mDrawer;

    private Toolbar toolbar;

    private CabinetFragment cabinetFragment;

    CustomPrimItem navCabinet;
    CustomPrimItem navPartner;

    private String isPartner;


    @Override
    public void startActivityForResult(Intent intent, int requestCode){
        intent.putExtra("requestCode", requestCode);
        super.startActivityForResult(intent, requestCode);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IabHelper mHelper = cabinetFragment.getHelper();
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d("TA", "onActivityResult handled by IABUtil.");
        }

        if(requestCode == CabinetFragment.CAB_NEW_OFFICE && data !=null) {
            final OfficeModel newOffice = new OfficeModel();
            newOffice.setAddress(data.getStringExtra("address"));
            newOffice.setLatitude(data.getDoubleExtra("latitude", 0));
            newOffice.setLongitude(data.getDoubleExtra("longitude", 0));
            newOffice.setCity(data.getStringExtra("city"));
            newOffice.setPhone(data.getStringExtra("phone"));
            newOffice.setStart_time(data.getStringExtra("start_time"));
            newOffice.setEnd_time(data.getStringExtra("end_time"));
            Cache.getCompany().suboffices.add(newOffice);
            cabinetFragment.officeList.getAdapter().notifyDataSetChanged();
            RestClient.getApi().addOffice(Cache.getAuth(), newOffice,
                    new Callback<HashMap<String, String>>() {
                        @Override
                        public void success(HashMap<String, String> stringStringHashMap, Response response) {
                            cabinetFragment.binding.setCompany(Cache.getCompany());
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.nav_catalog_label);
        setSupportActionBar(toolbar);

        cabinetFragment = new CabinetFragment();
        startSelection = 2;
        makeDrawer();
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
        startService(new Intent(this, PushService.class));

        String type = getIntent().getStringExtra("type");
        if (type != null) {
            String message = getIntent().getStringExtra("message");
            String reason = getIntent().getStringExtra("reason");
            switch (type) {
                case "2":
                    mDrawer.setSelection(1, true);
                    break;
                case "10":
                    break;
                default:
                    mDrawer.setSelection(5, true);
                    break;
            }
            MyAlert.getInstance().showMessage(this, message + "\n" + reason);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void makeDrawer() {
        String personName;
        if (Cache.getUser() != null) {
            personName = String.format("%s %s",
                    Cache.getUser().first_name,
                    Cache.getUser().last_name);
        } else {
            personName = getString(R.string.cabinet_title);
        }

        if(Cache.getCompany() != null) {
            isPartner = Cache.getCompany().legal_name;
        } else {
            isPartner = getString(R.string.nav_become_partner_label);
        }

        navCabinet = new CustomPrimItem()
                .withIdentifier(1)
                .withTextColorRes(R.color.material_drawer_dark_primary_text)
//                .withSelectedColorRes(R.color.md_grey_800)
                .withIcon(R.drawable.ic_person_outline_black_48dp)
                .withName(personName);

        SecondaryDrawerItem navCatalog = new SecondaryDrawerItem()
                .withIdentifier(2)
                .withTextColorRes(R.color.material_drawer_dark_hint_text)
                .withSelectedColorRes(R.color.md_grey_800)
                .withIcon(R.drawable.ic_transparent)
                .withName(R.string.nav_catalog_label);

        SecondaryDrawerItem navFavorites = new SecondaryDrawerItem()
                .withIdentifier(3)
                .withTextColorRes(R.color.material_drawer_dark_hint_text)
                .withSelectedColorRes(R.color.md_grey_800)
                .withIcon(R.drawable.ic_transparent)
                .withName(R.string.nav_favorites_label);

        SecondaryDrawerItem navMap = new SecondaryDrawerItem()
                .withIdentifier(4)
                .withTextColorRes(R.color.material_drawer_dark_hint_text)
                .withSelectedColorRes(R.color.md_grey_800)
                .withIcon(R.drawable.ic_transparent)
                .withName(R.string.nav_map_label);
//--------------------------------------------------------------------------------------//
        SecondaryDrawerItem navNewProm = new SecondaryDrawerItem()
                .withIdentifier(6)
                .withTextColorRes(R.color.material_drawer_dark_hint_text)
                .withSelectedColorRes(R.color.md_grey_800)
                .withIcon(R.drawable.ic_transparent)
                .withName(R.string.new_promotion_btn);

        SecondaryDrawerItem navGiveBonuses = new SecondaryDrawerItem()
                .withIdentifier(7)
                .withTextColorRes(R.color.material_drawer_dark_hint_text)
                .withSelectedColorRes(R.color.md_grey_800)
                .withIcon(R.drawable.ic_transparent)
                .withName("Начисление бонусов");

        navPartner = new CustomPrimItem()
                .withIdentifier(5)
                .withTextColorRes(R.color.material_drawer_dark_primary_text)
                .withSelectedColorRes(R.color.md_grey_800)
                .withIcon(R.drawable.ic_partner)
                .withName(isPartner);

        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHeader(R.layout.drawer_header)
                .withHeaderDivider(false)
                .withHeaderPadding(false)
                .withSliderBackgroundColorRes(R.color.drawer_main)
                .withStickyFooter(R.layout.drawer_footer)
                .withStickyFooterShadow(false)
                .withStickyFooterDivider(true)
                .withDisplayBelowStatusBar(true)
                .addDrawerItems(
                        navCabinet,
                        navCatalog,
                        navFavorites,
                        navMap,
                        navPartner
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        if (drawerItem.getIdentifier() == selection) {
                            mDrawer.closeDrawer();
                            return false;
                        }
                        switch (drawerItem.getIdentifier()) {
                            case 1:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.main_container, new UserCabinetFragment())
                                        .commit();
                                getSupportActionBar().setTitle(R.string.cabinet_title);
                                break;
                            case 2:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.main_container, new CategoriesFragment())
                                        .commit();
                                getSupportActionBar().setTitle(R.string.nav_catalog_label);
                                break;
                            case 3:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.main_container, new FavoritesFragment())
                                        .commit();
                                getSupportActionBar().setTitle("Избранное");
                                break;
                            case 4:
                                startActivity(new Intent(MainActivity.this, MapsActivity.class));
                                break;
                            case 5:
                                if (Cache.getCompany() != null) {
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.main_container, cabinetFragment)
                                            .commit();
                                    getSupportActionBar().setTitle(R.string.cabinet_title);
                                } else {
                                    startActivity(new Intent(MainActivity.this, NewCompanyActivity.class));
                                }
                                break;
                            case 6:
                                startActivity(new Intent(MainActivity.this, NewPromActivity.class));
                                break;
                            case 7:
                                startActivity(new Intent(MainActivity.this, GiveBonuses.class));
                                break;
                        }
                        selection = drawerItem.getIdentifier();
                        startSelection = selection;
                        return false;
                    }
                }).build();


        if(Cache.getCompany() != null) {
            mDrawer.addItems(navNewProm, navGiveBonuses);
        }

        ImageButton qrBtn = (ImageButton) mDrawer.getStickyFooter().findViewById(R.id.showQRCodeBtn);
        ImageButton barBtn = (ImageButton)mDrawer.getStickyFooter().findViewById(R.id.showBarcodeBtn);

        mDrawer.getStickyFooter().findViewById(R.id.exitBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Settings.clearData(MainActivity.this);

                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
        qrBtn.setOnClickListener(codeListener);
        barBtn.setOnClickListener(codeListener);

    }

    View.OnClickListener codeListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this, CodeActivity.class);
            switch (view.getId()) {
                case R.id.showQRCodeBtn:
                    intent.putExtra("format", "QR");
                    break;
                case R.id.showBarcodeBtn:
                    intent.putExtra("format", "bar");
                    break;
            }
            startActivity(intent);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        if (startSelection == 4 ||
            startSelection == 6 ||
            startSelection == 7) {
            startSelection = 2;
        }
            mDrawer.setSelection(startSelection, true);
    }

}

package tim.smedialink.orangesales.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.fragments.SignInFragment;
import tim.smedialink.orangesales.fragments.SignUpFragment;
import tim.smedialink.orangesales.fragments.SplashScreen;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.CompanyModel;
import tim.smedialink.orangesales.models.PartnerCompany;
import tim.smedialink.orangesales.models.UserJ;
import tim.smedialink.orangesales.network.OrangeCallback;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.gcm.RegistrationIntentService;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.NoInternetAlert;
import tim.smedialink.orangesales.utils.PushService;
import tim.smedialink.orangesales.utils.Settings;
import tim.smedialink.orangesales.utils.Trash;

public class SignActivity extends AppCompatActivity {

    public static final String SOCIAL_NETWORK_TAG = "SocialIntegrationMain.SOCIAL_NETWORK_TAG";
    private SignInFragment signInFragment;
    private ProgressDialog pd;

    public String soc_token = "";
    public String soc_type = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKCallback<VKAccessToken> callback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(final VKAccessToken res) {
                RestClient.getApi().soc_login(res.accessToken, "vk", new Callback<OrangeCallback>() {
                    @Override
                    public void success(OrangeCallback orangeCallback, Response response) {
                        Settings.saveData(SignActivity.this, C.TOKEN, orangeCallback.access_token);
                        startActivity(new Intent(SignActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        soc_token = res.accessToken;
                        soc_type = "vk";
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.sign_container, new SignUpFragment(), "regFragment")
                                .addToBackStack("login")
                                .commit();
                    }
                });
            }

            @Override
            public void onError(VKError error) {

            }
        };

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        signInFragment = new SignInFragment();
        RestClient.init();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.sign_container, new SplashScreen())
                .commit();

//        pd = new ProgressDialog(this);
//        pd.setMessage("Вход...");

        if (!Settings.readData(this, C.TOKEN).equals("")) {
//            pd.show();
            Cache.setAuth(Settings.readData(this, C.TOKEN));
            RestClient.getApi().getProfile(Cache.getAuth(),
                    new Callback<UserJ>() {
                        @Override
                        public void success(UserJ userJ, Response response) {
                            Trash.saveUser(SignActivity.this, userJ);
                            userJ.password = Settings.readData(SignActivity.this, C.PASSWORD);
                            Cache.setUser(userJ);
                            checkPartner(userJ.isPartner);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            new NoInternetAlert(SignActivity.this);
                        }
                    });
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.sign_container, signInFragment).addToBackStack("login").commit();
        }

    }

    @Override
    public void onBackPressed() {
        if (signInFragment.isVisible()) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void checkPartner(String isPartner) {
        String auth = "Bearer " + Settings.readData(this, C.TOKEN);
        if (isPartner.equals("true")) {
            RestClient.getApi().getCompany(auth, new Callback<CompanyModel>() {
                @Override
                public void success(CompanyModel company, Response response) {
                    Cache.setCompany(company);
                    Gson gson = new GsonBuilder().create();
                    String json = gson.toJson(company);
                    Settings.saveData(SignActivity.this, C.P_DATA, json);
                    startActivity(new Intent(SignActivity.this, MainActivity.class));
                    finish();
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        } else {
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}


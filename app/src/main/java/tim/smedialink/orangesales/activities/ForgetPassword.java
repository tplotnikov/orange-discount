package tim.smedialink.orangesales.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.fragments.Pass1Fragment;
import tim.smedialink.orangesales.fragments.Pass2Fragment;

public class ForgetPassword extends AppCompatActivity {

    public static String code = "";
    public static String passwordToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        Pass1Fragment firstFragment = new Pass1Fragment();
        Pass2Fragment secondFragment = new Pass2Fragment();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.pass_container, firstFragment)
                .commit();
    }
}

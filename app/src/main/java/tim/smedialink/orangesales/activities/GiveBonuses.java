package tim.smedialink.orangesales.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.models.PriceModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.windows.MyAlert;

public class GiveBonuses extends AppCompatActivity {

    private EditText price;
    private EditText discount;
    private TextView finalPrice;
    private TextView finalBonuses;

    private int bonusCourse;
    private int bonuses;
    private HashMap<String, String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_give_bonuses);

        getPrices();

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Начисление бонусов");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        price = (EditText)findViewById(R.id.giveBuySumm);
        discount = (EditText)findViewById(R.id.giveDiscountValue);
        finalPrice = (TextView)findViewById(R.id.giveSummText);
        finalBonuses = (TextView)findViewById(R.id.giveBonusesText);

        price.addTextChangedListener(watcher);
        discount.addTextChangedListener(watcher);

        ((TextView)findViewById(R.id.give_pLegalName)).setText(Cache.getCompany().getLegal_name());
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String needDate = sdf.format(date);
        ((TextView) findViewById(R.id.give_Date)).setText(needDate);

        findViewById(R.id.scanQRBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(GiveBonuses.this, ScanActivity.class), 199);
            }
        });

        final EditText num = (EditText)findViewById(R.id.giveCardNum);

        findViewById(R.id.acceptBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!finalPrice.getText().toString().equals("0") &&
                    !num.getText().toString().equals("0")) {
                    addBonuses();
                } else {
                    Toast.makeText(GiveBonuses.this, "Укажите номер карты и стоимость", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && data != null) {
            String cardNumber = data.getStringExtra("scannedResult");
            if (cardNumber != null && !cardNumber.equals("")) {
                ((EditText)findViewById(R.id.giveCardNum)).setText(cardNumber);
            } else {
                MyAlert.getInstance().showMessage(GiveBonuses.this, "Не удалось отсканировать.\nВведите код вручную");
            }
        }
    }

    private void getPrices() {
        RestClient.getApi().getPrices(Cache.getAuth(),
                new Callback<ArrayList<PriceModel>>() {
                    @Override
                    public void success(ArrayList<PriceModel> priceModels, Response response) {
                        for (PriceModel m : priceModels) {
                            if (m.getId().equals("bonus_course")) {
                                bonusCourse = new Double(m.getPrice()).intValue();
                                break;
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            Double priceValue = 0d;
            Integer discountValue;
            Double result;
            try {
                priceValue = Double.parseDouble(price.getText().toString());
                discountValue = Integer.parseInt(discount.getText().toString());
                result = priceValue - (priceValue / 100) * discountValue;
            } catch (Exception e) {
                discountValue = 0;
                result = priceValue - (priceValue/100)*discountValue;
            }
            finalPrice.setText(String.valueOf(result));
            finalBonuses.setText(String.valueOf((priceValue / 100) * discountValue * bonusCourse));
            bonuses = ((Double)(((priceValue / 100) * discountValue) * bonusCourse)).intValue();

        }
    };

    private void addBonuses() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Подождите..");
        pd.show();
        data = new HashMap<>();
        data.put("username", ((EditText)findViewById(R.id.giveCardNum)).getText().toString());
        data.put("bonuses", String.valueOf(bonuses));
        RestClient.getApi().addBonuses(Cache.getAuth(), data,
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        pd.dismiss();
                        MyAlert.getInstance().showMessage(GiveBonuses.this, "Бонусы начислены!");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if(error.toString().contains("java.io.EOFException")){
                            addBonuses();
                        }
                    }
                });
    }
}

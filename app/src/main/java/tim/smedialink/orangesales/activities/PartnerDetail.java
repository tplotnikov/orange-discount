package tim.smedialink.orangesales.activities;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.AddrAdapter;
import tim.smedialink.orangesales.databinding.ActivityPartnerDetailBinding;
import tim.smedialink.orangesales.fragments.PromDetail;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.CompanyModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.LinearLayoutManager;
import tim.smedialink.orangesales.windows.MyAlert;

public class PartnerDetail extends AppCompatActivity {

    ActivityPartnerDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_partner_detail);
        if (PromDetail.doNotCompany != null) {
            binding.setCompany(PromDetail.doNotCompany);
        } else {
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setMessage("Подождите...");
            pd.show();
            RestClient.getApi().getAnyCompany(Cache.getAuth(),
                    getIntent().getStringExtra("companyId"), new Callback<CompanyModel>() {
                        @Override
                        public void success(CompanyModel companyModel, Response response) {
                            binding.setCompany(companyModel);
                            ImageView logo = (ImageView)findViewById(R.id.d_companyLogo);

                            Picasso.with(PartnerDetail.this)
                                    .load(C.UPLOADS_BASE_URL+binding.getCompany().logoUrl)
                                    .into(logo);

                            RecyclerView officesList = (RecyclerView)findViewById(R.id.d_companyOfficesList);
                            LinearLayoutManager manager = new LinearLayoutManager(PartnerDetail.this);
                            AddrAdapter addrAdapter = new AddrAdapter(PartnerDetail.this, binding.getCompany().suboffices);
                            officesList.setLayoutManager(manager);
                            officesList.setAdapter(addrAdapter);
                            pd.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            pd.dismiss();
                            MyAlert.getInstance().showMessage(PartnerDetail.this, "Данные о партнере не загружены");
                        }
                    });
        }

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Партнер");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}

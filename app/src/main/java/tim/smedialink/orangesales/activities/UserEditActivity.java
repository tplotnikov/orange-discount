package tim.smedialink.orangesales.activities;

import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.databinding.ActivityUserEditBinding;
import tim.smedialink.orangesales.geo.CityAdapter;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.Settings;
import tim.smedialink.orangesales.windows.MyAlert;

public class UserEditActivity extends AppCompatActivity {

    ActivityUserEditBinding binding;

    private EditText userEditName;
    private EditText userEditSurname;
    private EditText userEditPhone;
    private Spinner userEditAge;
    private Spinner userEditGender;
    private AutoCompleteTextView userEditCity;

    private EditText userOldPass;
    private EditText userNewPass;
    private EditText userNewPassAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_edit);
        binding.setUser(Cache.getUser());

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Редактирование");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        userEditName = (EditText)findViewById(R.id.editUserName);
        userEditSurname = (EditText)findViewById(R.id.editUserSurname);
        userEditPhone = (EditText)findViewById(R.id.editUserPhone);
        userEditPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        userEditCity = (AutoCompleteTextView)findViewById(R.id.editUserCity);
        userEditCity.setDropDownWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        userOldPass = (EditText)findViewById(R.id.userOldPass);
        userNewPass = (EditText)findViewById(R.id.userNewPass);
        userNewPassAgain = (EditText)findViewById(R.id.userNewPassAgaing);

        CityAdapter adapter = new CityAdapter(this);
        userEditCity.setAdapter(adapter);
        userEditCity.setThreshold(1);

        findViewById(R.id.editUserSaveBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(collectData()) {
                    RestClient.getApi().editProfile(Cache.getAuth(),
                            binding.getUser().getUser(),
                            new Callback<Response>() {
                                @Override
                                public void success(Response response, Response response2) {
                                    Settings.saveData(UserEditActivity.this, C.PASSWORD, userNewPass.getText().toString());
                                    Cache.setUser(binding.getUser());
                                    MyAlert.getInstance().showMessage(UserEditActivity.this, "Профиль обновлен");
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    MyAlert.getInstance().showMessage(UserEditActivity.this, "Произошла ошибка.\nПопробуйте позднее");
                                }
                            });
                }
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        userEditAge = (Spinner)findViewById(R.id.editUserAge);
        userEditAge.setSelection(binding.getUser().age);
        userEditGender = (Spinner)findViewById(R.id.editUserGender);
        userEditGender.setSelection(binding.getUser().gender);
    }

    private boolean collectData() {
        binding.getUser().first_name = userEditName.getText().toString();
        binding.getUser().last_name = userEditSurname.getText().toString();
        binding.getUser().gender = userEditGender.getSelectedItemPosition();
        binding.getUser().age = userEditAge.getSelectedItemPosition();
        binding.getUser().city_string = userEditCity.getText().toString();
        binding.getUser().phone = userEditPhone.getText().toString().replaceAll("\\D", "");


        //TODO: Вылетает тут иногда
        if (!userOldPass.getText().toString().isEmpty()) {
            if (Settings.readData(this, C.PASSWORD).equals(userOldPass.getText().toString())) {

                if (!userNewPass.getText().toString().equals(userNewPassAgain.getText().toString())) {
                    Snackbar.make(userOldPass, "Пароли не совпадают", Snackbar.LENGTH_LONG).show();
                    return false;
                } else {
                    binding.getUser().password = userNewPass.getText().toString();
                    return true;
                }
            } else {
                Snackbar.make(userOldPass, "Введен неверный пароль", Snackbar.LENGTH_LONG).show();
                return false;
            }
        } else {
            return true;
        }
    }
}

package tim.smedialink.orangesales.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.SimpleAdapter;
import tim.smedialink.orangesales.fragments.FirstRegPage;
import tim.smedialink.orangesales.fragments.SecondRegPage;
import tim.smedialink.orangesales.models.C;
import tim.smedialink.orangesales.models.OfficeModel;
import tim.smedialink.orangesales.models.PartnerCompany;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.Settings;


public class NewCompanyActivity extends AppCompatActivity {

    public static final int PICK_LOGO_CODE = 1001;
    public static final int ADD_OFFICE_CODE = 1002;
    public static final int CHOOSE_CATS_CODE = 1003;

    public static HashMap<String, Object> data;

    public Bitmap logo;
    private TypedFile tLogo;

    public FirstRegPage firstRegPage;
    public SecondRegPage secondRegPage;

    public ArrayList<OfficeModel> offices = new ArrayList<>();
    public ArrayList<String> catNames = new ArrayList<>();

    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_company);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.nav_become_partner_label);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(navClick);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firstRegPage = new FirstRegPage();
        secondRegPage = new SecondRegPage();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.p_reg_container, firstRegPage)
                .commit();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode){
        intent.putExtra("requestCode", requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(intent != null) {
            switch (requestCode) {
                case PICK_LOGO_CODE:
                    try {
                        InputStream inputStream = getApplicationContext().getContentResolver().openInputStream(intent.getData());
                        logo = BitmapFactory.decodeStream(inputStream);
                        secondRegPage.comLogo.setImageBitmap(logo);

                        Uri selectedImage = intent.getData();
                        String[] filePathColumn = { MediaStore.Images.Media.DATA };

                        Cursor cursor = getContentResolver().query(selectedImage,
                                filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String pathImage = cursor.getString(columnIndex);
                        cursor.close();
                        tLogo = new TypedFile("image/jpeg", new File(pathImage));
                    } catch (FileNotFoundException | NullPointerException e) {
                        e.printStackTrace();
                    }
                    break;

                case ADD_OFFICE_CODE:
                    OfficeModel officeModel = new OfficeModel();
                    officeModel.setAddress(intent.getStringExtra("address"));
                    officeModel.setCity(intent.getStringExtra("city"));
                    officeModel.setLatitude(intent.getDoubleExtra("latitude", 0));
                    officeModel.setLongitude(intent.getDoubleExtra("longitude", 0));
                    officeModel.setPhone(intent.getStringExtra("phone"));
                    officeModel.setStart_time(intent.getStringExtra("start_time"));
                    officeModel.setEnd_time(intent.getStringExtra("end_time"));
                    offices.add(officeModel);
                    secondRegPage.adapter.notifyDataSetChanged();
                    break;

                case CHOOSE_CATS_CODE:
                    catNames = intent.getStringArrayListExtra("names");
                    ArrayList<String> catIds = intent.getStringArrayListExtra("ids");
                    data.put("cats", catIds);
                    secondRegPage.catsAdapter = new SimpleAdapter(catNames);
                    secondRegPage.pCatsList.setAdapter(secondRegPage.catsAdapter);
                    break;
            }
        }
    }


    public View.OnClickListener newCompanyClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            pd = new ProgressDialog(NewCompanyActivity.this);
            pd.setMessage("Отправка данных, подождите...");
            pd.show();

            data.put("addresses", secondRegPage.adapter.getRecords());
            data.put("comAdditions", secondRegPage.comAdditions.getText().toString());
            data.put("comDescr", secondRegPage.comDescr.getText().toString());
            data.put("comSiteUrl", secondRegPage.comSiteUrl.getText().toString());

            final PartnerCompany company = new PartnerCompany();
            company.fillWithData(NewCompanyActivity.this, data);

            RestClient.getApi().createCompany(Cache.getAuth(), company,
                    new Callback<PartnerCompany>() {
                        @Override
                        public void success(PartnerCompany company1, Response response2) {
                            Settings.saveData(getBaseContext(), C.IS_PARTNER, "wait");
                            setLogo(company1);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("COMPANY", error.getMessage());
                            showModal(getString(R.string.p_reg_error_message));
                            pd.dismiss();
                        }
                    });
        }
    };

    private void setLogo(PartnerCompany com) {
        RestClient.getApi().setLogo(Cache.getAuth(),com.id, tLogo, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                Settings.saveData(getBaseContext(), C.IS_PARTNER, "wait");
                showModal(getString(R.string.p_reg_message));
                pd.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                showModal(getString(R.string.p_reg_message));
                pd.dismiss();
            }
        });
    }

    private void showModal(final String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(message)
                .setNeutralButton("Хорошо", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(message.equals(getString(R.string.p_reg_message)))
                            finish();
                        else
                            dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private View.OnClickListener navClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };
}

package tim.smedialink.orangesales.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import tim.smedialink.orangesales.R;
import tim.smedialink.orangesales.adapters.SimpleAdapter;
import tim.smedialink.orangesales.models.MailoutModel;
import tim.smedialink.orangesales.network.RestClient;
import tim.smedialink.orangesales.utils.Cache;
import tim.smedialink.orangesales.utils.LinearLayoutManager;
import tim.smedialink.orangesales.windows.MyAlert;

public class SMSSend extends AppCompatActivity {

    public static final int ADD_AGES_CODE = 3001;
    public static final int ADD_CITIES_CODE = 3002;

    private CheckedTextView publicCheck;
    private Button smsAcceptBtn;
    private Spinner ageSpinner;

    private RecyclerView agesList;
    private RecyclerView citiesList;
    private LinearLayoutManager agesManager;
    private LinearLayoutManager citiesManager;
    private SimpleAdapter agesAdapter;
    private SimpleAdapter citiesAdapter;

    private ArrayList<String> citiesIds = new ArrayList<>();
    private ArrayList<String> agesIds = new ArrayList<>();
    private ArrayList<String> genders = new ArrayList<>();

    TabLayout tabs;

    int type = 0;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            switch (requestCode) {
                case ADD_AGES_CODE:
                    agesList = (RecyclerView)findViewById(R.id.agesList);
                    agesManager = new LinearLayoutManager(this);
                    ArrayList<String> ages = data.getStringArrayListExtra("names");
                    agesIds = data.getStringArrayListExtra("ids");
                    agesAdapter = new SimpleAdapter(ages);
                    agesList.setLayoutManager(agesManager);
                    agesList.setAdapter(agesAdapter);
                    break;
                case ADD_CITIES_CODE:
                    citiesList = (RecyclerView)findViewById(R.id.citiesList);
                    citiesManager = new LinearLayoutManager(this);
                    ArrayList<String> cities = data.getStringArrayListExtra("names");
                    citiesIds = data.getStringArrayListExtra("ids");
                    citiesAdapter = new SimpleAdapter(cities);
                    citiesList.setLayoutManager(citiesManager);
                    citiesList.setAdapter(citiesAdapter);
            }
            calculateCost();
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode){
        intent.putExtra("requestCode", requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smssend);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Заказать рассылку");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        makeTabs();

        smsAcceptBtn = (Button)findViewById(R.id.smsAcceptBtn);
        smsAcceptBtn.setEnabled(false);
        publicCheck = (CheckedTextView)findViewById(R.id.publicCheck);
        publicCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publicCheck.setChecked(!publicCheck.isChecked());
                smsAcceptBtn.setEnabled(publicCheck.isChecked());
            }
        });

        findViewById(R.id.addAgesBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SMSSend.this, CheckCats.class), ADD_AGES_CODE);
            }
        });
        findViewById(R.id.addCitiesBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(SMSSend.this, CheckCats.class), ADD_CITIES_CODE);
            }
        });
        ageSpinner = (Spinner)findViewById(R.id.ageSpinner);
        ageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genders = new ArrayList<String>();
                if (position != 2) {
                    genders.add(String.valueOf(position));
                } else {
                    genders.add("0");
                    genders.add("1");
                }
                calculateCost();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ageSpinner.setSelection(2);

        findViewById(R.id.smsAcceptBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeMailout();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        smsAcceptBtn.setEnabled(publicCheck.isChecked());
    }

    private void calculateCost() {
        RestClient.getApi().getMailoutCost(Cache.getAuth(),
                type,
                citiesIds,
                agesIds,
                genders, new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> stringStringHashMap, Response response) {
                        ((TextView) findViewById(R.id.smsCount)).setText(stringStringHashMap.get("amount"));
                        ((TextView) findViewById(R.id.smsSumText)).setText(stringStringHashMap.get("cost"));
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    private void makeMailout() {
        String smsText = ((TextView)findViewById(R.id.smsText)).getText().toString();
        String smsAdditions = ((TextView)findViewById(R.id.smsAdditions)).getText().toString();

        final MailoutModel mailout = new MailoutModel();
        mailout.setType(type);
        mailout.setMessage(smsText);
        mailout.setDescription(smsAdditions);
        HashMap<String, ArrayList<String>> conditions = new HashMap<>();
        conditions.put("FK_city", citiesIds);
        conditions.put("age", agesIds);
        conditions.put("gender", genders);
        mailout.setConditions(conditions);

        RestClient.getApi().createMailout(Cache.getAuth(),
                mailout, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        Cache.getCompany().mailouts.add(mailout);
                        MyAlert.getInstance().showMessage(SMSSend.this, "Заявка отправлена");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if(error.toString().contains("java.io.EOFException")){
                            makeMailout();
                        }
                        MyAlert.getInstance().showMessage(SMSSend.this, error.getMessage());
                    }
                });
    }

    private void makeTabs() {
        tabs = (TabLayout)findViewById(R.id.tabs);

        TabLayout.Tab standartTab = tabs.newTab()
                .setText("PUSH");
        TabLayout.Tab premiumTab = tabs.newTab()
                .setText("SMS");

        tabs.addTab(standartTab);
        tabs.addTab(premiumTab);

        tabs.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tabs.setTabTextColors(getResources().getColor(R.color.et_divider_not_focused),
                getResources().getColor(R.color.material_drawer_dark_primary_text));

        final TextView descr = (TextView) findViewById(R.id.textView70);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        descr.setText(getString(R.string.push_decr));
                        type = 0;
                        break;
                    case 1:
                        descr.setText(getString(R.string.sms_decr));
                        type = 1;
                        break;
                }
                calculateCost();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        descr.setText(getString(R.string.push_decr));
                        type = 0;
                        break;
                    case 1:
                        descr.setText(getString(R.string.sms_decr));
                        type = 1;
                        break;
                }
                calculateCost();
            }
        });
    }
}

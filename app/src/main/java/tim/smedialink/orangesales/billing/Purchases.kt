package tim.smedialink.orangesales.billing

import android.app.Activity
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit.Callback
import retrofit.RetrofitError
import retrofit.client.Response
import tim.smedialink.orangesales.R
import tim.smedialink.orangesales.activities.MainActivity
import tim.smedialink.orangesales.models.C
import tim.smedialink.orangesales.network.RestClient
import tim.smedialink.orangesales.utils.Cache
import tim.smedialink.orangesales.utils.Settings
import java.util.*

class Purchases(activity: Activity) {

    var mSKU: String = ""
    val mActivity = activity
    val pubKey: String = mActivity.getString(R.string.public_key)

    var mPurchase: Purchase? = null
    var purchaseData: String? = null
    var device_os: String? = null
    var signature: String? = null

    private var balance: String? = null

    public var mHelper = IabHelper(mActivity, pubKey)

    public fun setupHelper(): Unit {
        //Инициализация хелпера для работы с Play Store
        mHelper.startSetup { result ->
            if (result != null && result.isSuccess) {
                Log.d("Billing Setup", "Success")
            } else {
                Log.d("Billing Setup", "error")
            }
        }
    }

    //Оплата покупки по нажатию на кнопку
    public fun buyItem(sku: String) {
        mSKU = sku;

        Log.d("buyItem", sku)
        if (!mHelper.mAsyncInProgress) {
            mHelper.launchPurchaseFlow(mActivity, mSKU, 10001,
                    getBuyListener(), Settings.readData(mActivity.baseContext, C.USERNAME))
        }
    }

    //Валидирование транзакции на внутреннем сервере
     fun validatePurchase() {
        RestClient.getApi().makePurchase(Cache.getAuth(),
                device_os, purchaseData, signature,
                object : Callback<HashMap<String, String>> {
                    override fun success (strings: HashMap<String, String>, response: Response) {
                        Log.d("validate", strings["value"])
                        balance = strings["value"]
                        mHelper.consumeAsync(mPurchase, getConsumeListener())
                    }

                    override fun failure(error: RetrofitError) {
                        val gson: Gson = GsonBuilder().create()
                        val purchaseJson: String = gson.toJson(mPurchase)
                        Settings.saveData(mActivity, "temp_purchase", purchaseJson)
                        mHelper.consumeAsync(mPurchase, getConsumeListener());
                    }
                })
    }

    //Слушатель на покупку в Google Play
    fun getBuyListener(): IabHelper.OnIabPurchaseFinishedListener =
            IabHelper.OnIabPurchaseFinishedListener { result, purchase ->
                Log.d("buyListener result", result.isSuccess.toString())

                if(result.isFailure) {
                    Toast.makeText(mActivity, "Покупка не удалась, попробуйте позднее",
                            Toast.LENGTH_LONG).show()
                } else {

                    if (purchase.sku == mSKU) {
                        mPurchase = purchase
                        signature = purchase.signature
                        purchaseData = purchase.mOriginalJson
                        device_os = "android"
                        Log.d("buyListener", result.message)
                        validatePurchase()
                    }
                }
            }

    //Слушатель на потребление(consume) покупки
    fun getConsumeListener(): IabHelper.OnConsumeFinishedListener =
            IabHelper.OnConsumeFinishedListener { purchase, iabResult ->
                if (iabResult.isSuccess) {
                    Log.d("consume", iabResult.message)
                    Settings.saveData(mActivity, C.PARTNER_BALANCE, balance)
                } else {
                    Toast.makeText(mActivity, "Покупка не удалась, попробуйте позднее",
                            Toast.LENGTH_LONG).show()
                }
            }
}
